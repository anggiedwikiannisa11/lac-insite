<section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?php echo $nama; ?></p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li>
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-home"></i> <span>Dashboard</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			<?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==2){ ?>
            <li>
              <a href="<?php echo base_url(); ?>jabatan">
                <i class="fa fa-tag"></i> <span>Jabatan</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			
			
            <li>
              <a href="<?php echo base_url(); ?>karyawan">
                <i class="fa fa-photo"></i> <span>Karyawan</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			<?php } ?>
           <!-- <li>
              <a href="<?php echo base_url(); ?>absensi">
                <i class="fa fa-print"></i> <span>Absensi</span> <small class="label pull-right bg-green"></small>
              </a>
            </li> -->
			<?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==3 || $this->session->userdata('level') ==4){ ?>
			 <li>
              <a href="<?php echo base_url(); ?>test">
                <i class="fa fa-archive"></i> <span>Data Test EPrT</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			<?php } ?>
			
			<?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==3 || $this->session->userdata('level') ==4){ ?>
			 <li>
              <a href="<?php echo base_url(); ?>testecct">
                <i class="fa fa-archive"></i> <span>Data Test ECCT</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			<?php } ?>
			<?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==3 || $this->session->userdata('level') ==4){ ?>
			 <li>
              <a href="<?php echo base_url(); ?>testecctsmktel">
                <i class="fa fa-archive"></i> <span>Data Test ECCT SMK Telkom</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			<?php } ?>
			
			<?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==3 || $this->session->userdata('level') ==4){ ?>
			 <li>
              <a href="<?php echo base_url(); ?>testeprtittp">
                <i class="fa fa-archive"></i> <span>Data Test EPrT ITTP</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			<?php } ?>
			
			<?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==3 || $this->session->userdata('level') ==4){ ?>
			 <li>
              <a href="<?php echo base_url(); ?>testeprtsttpln">
                <i class="fa fa-archive"></i> <span>Data Test EPrT STTPLN</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			<?php } ?>
			<?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==3 || $this->session->userdata('level') ==4){ ?>
			 <li>
              <a href="<?php echo base_url(); ?>testeprtsmatelbdg">
                <i class="fa fa-archive"></i> <span>Data Test EPrT SMA Telkom Bandung</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
			<?php } ?>

			<li>
                <a target="_blank" href="<?php echo base_url(); ?>laporan/cek_qr/"><i class="fa fa-qrcode"></i> <span>Cek Serial Code Sertifikat</span> <small class="label pull-right bg-green"></small></a>
            </li>
			<!-- <li class="treeview"><a href="#"><i class="fa fa-book"></i> <span>Permohonan Cuti</span><i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url(); ?>cuti">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right"></i> Pengajuan</a></li>
						<li><a href="<?php echo base_url(); ?>cuti/historycuti">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right"></i> History</a></li>
					</ul>
			</li> -->
<!-- add by Anggie -->
      <?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==5){ ?>
            <li>
              <a href="<?php echo base_url(); ?>Pengajar">
                <i class="fa fa-pencil"></i> <span>Iput Scheduling</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
      <?php } ?>
      <?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==5){ ?>
            <li>
              <a href="<?php echo base_url(); ?>kelas">
                <i class="fa fa-home"></i> <span>Class Status</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
      <?php } ?>
      <?php if($this->session->userdata('level') ==1){ ?>
            <li>
              <a href="<?php echo base_url(); ?>grafik">
                <i class="fa fa-bar-chart"></i> <span>Grafik Score</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
      <?php } ?>
      <?php if($this->session->userdata('level') ==1){ ?>
      <li>
              <a href="<?php echo base_url(); ?>user">
                <i class="fa fa-user"></i> <span>User</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
      <?php } ?>
      <li>
              <a href="<?php echo base_url(); ?>login/logout">
                <i class="fa fa-sign-out"></i> <span>Keluar</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
            <li class="header">Piledriver Waltz</li>
            <!-- <li><a href="#"><i class="fa fa-circle-o text-danger"></i> Important</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-warning"></i> Warning</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-info"></i> Information</a></li> -->
          </ul>
        </section>