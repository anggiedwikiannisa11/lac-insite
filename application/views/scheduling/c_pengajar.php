<!DOCTYPE html>
<html lang="en">
<head>
  <title>LaC INSTRUCTOR'S SCHEDULE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<style>
    thead{
        text-align: center;
    }
</style>

<div class="container">
  <center><h2>LaC INSTRUCTOR'S SCHEDULE 2019</h2></center>
  <!-- <form action="<?= base_url().'index.php/pengajar/inputdata'?>" method="POST"> -->
  	<!--  <div class="form-group">
  		<label for="text">No</label>
      <input type="text" disabled class="form-control" id="no" placeholder="">
    </div>-->
<table class="table table-hover">
    <thead class="thead-dark">
        <tr>
            <th rowspan="2" style="vertival-align:middle;text-align:center;">NO</th>
            <th rowspan="2" style="vertival-align:middle;text-align:center;">PROGRAM</th>
            <th rowspan="2" style="vertival-align:middle;text-align:center;">TYPE OF PROGRAM</th>
            <th colspan="3">MONDAY</th>
            <th colspan="3">TUESDAY</th>
            <th colspan="3">WEDNESDAY</th>
            <th colspan="3">THURSDAY</th>
            <th colspan="3">FRIDAY</th>
        </tr>
        <tr>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
        </tr>   
    </thead>
    <tbody>
       
    </tbody>
</table>
</body>
</html>
