<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TestEprtSttpln extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->helper('currency_format_helper');
		$this->load->library('excel');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url().'backend');
		}
	}

	public function index()
	{

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			//'data_test' => $this->model->GetDataTest("order by id_test desc")->result_array(),
			'data_test' => array(),
		);

		$this->load->view('test/data_test_eprt_sttpln', $data);
	}
	
	public function viewTest()
	{
		$tipeTes = $this->input->post("tipeTes");
		$tanggalTesEx1 = $this->input->post("tanggalTesEx1");
		$tanggalTesEx2 = $this->input->post("tanggalTesEx2");
		$waktuTes = $this->input->post("waktuTes");
		$keterangan = $this->input->post("keterangan");
		$nim_nama = $this->input->post("nim_nama");
		
		$where ="";
		$count=0;
		
		if($tipeTes !="")
		{
			$where .=" and type_test = '$tipeTes'";
			$count++;
		}		
		
		if($tanggalTesEx1 !="")
		{
			if($count > 0  && $waktuTes == ""){
			
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tanggal_test) = '$tanggalTesEx1'";
				}
			}
			else if($count == 0  && $waktuTes == "")
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tanggal_test) = '$tanggalTesEx1'";
				}
			}
			
			elseif($count > 0  && $waktuTes != ""){

				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' and Time(tanggal_test) = '$waktuTes' ";
				}
				else
				{
					$tanggalTestAll = $tanggalTes." ".$waktuTes;
					$where .=" and tanggal_test = '$tanggalTestAll'";
				}
			}
			else if($count == 0  && $waktuTes != "")
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' and Time(tanggal_test) = '$waktuTes' ";
				}
				else
				{
					$tanggalTestAll = $tanggalTes." ".$waktuTes;
					$where .=" and tanggal_test = '$tanggalTestAll'";
				
				}
			}
		}
		
		if($keterangan != "")
		{
			if($count > 0)
			{
				$where .= "and keterangan = $keterangan";
			}
			else
			{
				$where .= "and keterangan = $keterangan";
			}
		}
		
		if($nim_nama != "")
		{
			if($count > 0)
			{
				$where .= "and nim_nik like '%$nim_nama%' or nama_lengkap like '%$nim_nama%'";
			}
			else
			{
				$where .= "and nim_nik like '%$nim_nama%' or nama_lengkap like '%$nim_nama%'";
			}
		}

		//var_dump($where);die();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_test' => $this->model->GetDataTestEprtSttpln($where)->result_array(),
		);

		$this->load->view('test/data_test_eprt_sttpln', $data);
	}
	
	public function import()
	{
	  if(isset($_FILES["file"]["name"]))
	  {
	   $path = $_FILES["file"]["tmp_name"];
	   $object = PHPExcel_IOFactory::load($path);
	   $sheet = $object->getActiveSheet()->toArray(null, true, true ,true);
          $highestRow = sizeof($sheet);
//          echo $highestRow;
//          die();
          // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
          $data = array();

          $numrow = 1;
          foreach($sheet as $row){
              // Cek $numrow apakah lebih dari 1
              // Artinya karena baris pertama adalah nama-nama kolom
              // Jadi dilewat saja, tidak usah diimport
              if($numrow > 1 and $numrow<=$highestRow){
                  // Kita push (add) array data ke variabel data
                  array_push($data, array(
                      'nim_nik'   => $row['B'],
                      'nama_lengkap'  => $row['C'],
                      'type_test' => 'EPrT',
                      'lc'  => $row['D'],
                      'gc'  => $row['E'],
                      'rc'  => $row['F'],
                      'clc'  => $row['G'],
					  'cgc'  => $row['H'],
					  'crc'  => $row['I'],
                      'total'  => $row['J'],
                      'tanggal_test' =>$row['K'],
                      'user_modified'=>$this->session->userdata('id_user')

                  ));
              }

              $numrow++; // Tambah 1 setiap kali looping
          }
	   	/* $noser=$this->model->GetLastNoCertificateEprtSttpln();
		//var_dump($noser);die();
		$noSertifikat =0;
		$pos = 0;
	   foreach($object->getWorksheetIterator() as $worksheet)
	   {
		$highestRow = $worksheet->getHighestRow();
		$highestColumn = $worksheet->getHighestColumn();
		
		$rowTipeTes = $worksheet->getCellByColumnAndRow(1, 1)->getValue();
		$rowTanggalTes = $worksheet->getCellByColumnAndRow(1, 2)->getValue();
		$rowWaktuTes = $worksheet->getCellByColumnAndRow(1, 3)->getValue();
		$rowRuangTes = $worksheet->getCellByColumnAndRow(1, 4)->getValue();
		
		$rowTipeTesExp = explode(":",$rowTipeTes );
		$rowTanggalTesExp = explode(":",$rowTanggalTes );
		$rowWaktuTesExp = explode(":",$rowWaktuTes );
		$rowRuangTesExp = explode(":",$rowRuangTes );
		
		$rowTanggalTesExp = explode(" ",$rowTanggalTesExp[1]);
		$montNum = $this->model->GetIndonesianDateNameToNumber($rowTanggalTesExp[2]);
		
		
		//var_dump($pos,$noSertifikat);
		
		$tipeTes_ = trim($rowTipeTesExp[1]);		
		$waktuTes_ =$rowWaktuTesExp[1].":".$rowWaktuTesExp[2];
		$tanggalTes_ =$rowTanggalTesExp[3]."-".$montNum."-".$rowTanggalTesExp[1]." ".$waktuTes_;
		$ruangTes_ = trim($rowRuangTesExp[1]);
		$userId = $this->session->userdata('id_user');
		//var_dump($highestRow);die();
		for($row=7; $row<=$highestRow; $row++)
		{
		 $nim_nik = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
		 $fakultas = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
		 $prodi = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
		 $nama = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
		 $lc = $worksheet->getCellByColumnAndRow(6, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(6, $row)->getValue();
		 $gc = $worksheet->getCellByColumnAndRow(7, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(7, $row)->getValue();
		 $rc = $worksheet->getCellByColumnAndRow(8, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(8, $row)->getValue();
		 $clc = $worksheet->getCellByColumnAndRow(9, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(9, $row)->getValue();
		 $cgc = $worksheet->getCellByColumnAndRow(10, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(10, $row)->getValue();
		 $crc = $worksheet->getCellByColumnAndRow(11, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(11, $row)->getValue();
		 $total = $worksheet->getCellByColumnAndRow(12, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(12, $row)->getValue();
		 $keterangan = $worksheet->getCellByColumnAndRow(13, $row)->getValue()==""?null:$worksheet->getCellByColumnAndRow(13, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(13, $row)->getValue();
		 
		 if($noser->noser==null && $noSertifikat ==0)
		{			
			$noSertifikat =SERTIFIKAT_EPRT_STTPLN_NO;	
		}
		else if($pos==0 &&$noser->noser != null)
		{
			$noSertifikat =$noser->noser;
			$noSertifikat = $noSertifikat +1;
			$pos++;
			//var_dump($pos);die();
		}
		else 
		{
			$noSertifikat = $noSertifikat +1;
		}
		
		 $data[] = array(
		  'no_sertifikat'  => $noSertifikat,
		  'tanggal_test'  => $tanggalTes_,
		  'nim_nik'   => $nim_nik,
		  'fakultas'    => $fakultas,
		  'prodi'  => $prodi,
		  'nama_lengkap'  => $nama,
		  'type_test'  => $tipeTes_,
		  'ruang'  => $ruangTes_,
		  'lc'  => $lc,
		  'gc'  => $gc,
		  'rc'  => $rc,
		  'clc'  => $clc,
		  'cgc'  => $cgc,
		  'crc'  => $crc,
		  'total'  => $total,
		  'keterangan'  => $keterangan,
		  'user_modified'=>$userId
		 );
		}
	   } */
	   $this->model->Insert("tb_test_eprt_sttpln",$data);
	  // echo 'Data Imported successfully';
	}
	$this->session->set_flashdata('success','Data berhasil di input');
	redirect('testeprtsttpln');

 }
public function get_test_detail()
 {
        $phoneData = $this->input->post('phoneData');
		
        if(isset($phoneData) and !empty($phoneData)){
            $records = $this->model->GetDataTestEprtSttpln(" and nim_nik ='$phoneData'")->result_array();
			//var_dump($records);die();
            $output = ' 
         <div class="col-lg-12">
          <table class="table table-bordered">
           <tr>
            <td>Tanggal Tes</td>
            <td>NIM/NIK</td>
			<td>fakultas</td>
			<td>prodi</td>
			<td>Nama</td>
			<td>Tipe</td>
			<td>Ruang</td>
			<td>lc</td>
			<td>gc</td>
			<td>rc</td>
			<td>clc</td>
			<td>cgc</td>
			<td>crc</td>
			<td>total</td>
			<td>id sertifikat</td>
			<td>No Sertifikat</td>			
           </tr>';
            foreach($records as $row){
             $output .= '      
           <tr>
            <td>'.$row["tanggal_test"].'</td>
            <td>'.$row["nim_nik"].'</td> 
			<td>'.$row["fakultas"].'</td>
			<td>'.$row["prodi"].'</td>
			<td>'.$row["nama_lengkap"].'</td>
			<td>'.$row["type_test"].'</td>
			<td>'.$row["ruang"].'</td>
			<td>'.$row["lc"].'</td>
			<td>'.$row["gc"].'</td>
			<td>'.$row["rc"].'</td>
			<td>'.$row["clc"].'</td>
			<td>'.$row["cgc"].'</td>
			<td>'.$row["crc"].'</td>
			<td>'.$row["total"].'</td>
			<td>'.$row["no_sertifikat"].'</td>
			<td>'.$row["no_sertifikat_comp"].'</td>
           </tr>                           

	  ';
            }
			$output .='</table>
					</div>';
            echo $output;
        }
        else {
         echo '<center><ul class="list-group"><li class="list-group-item">'.'Select a Phone'.'</li></ul></center>';
        }
	}

  public function validasiSertifikat()
  {
	$idTest = $this->uri->segment(3);
	
	$dataSertifikat = $this->model->GetDataTestEprtSttpln(" and a.id_test=$idTest")->first_row();
	$jenisSertifikat =SERTIFIKAT_EPRT_STTPLN;
	if($dataSertifikat->cetakan_ke ==null || $dataSertifikat->cetakan_ke==0)
	{
		$cetakan =1;
	}
	else
	{
		$cetakan = $dataSertifikat->cetakan_ke;
		$cetakan = $cetakan+1;
	}
	$noSertifikat = sprintf("%04d", $dataSertifikat->no_sertifikat);
	$noSertifikatComp = $jenisSertifikat.". ".$noSertifikat."/BHS.0/".date("Y");
	$data[]=array(
			"cetakan_ke" => $cetakan,
			"id_test"=> $idTest,
			"no_sertifikat_comp" => $noSertifikatComp
			);
	$this->model->UpdateTestEprtSttpln($data[0]);
	
	$dataV = array
			(
			'nama' => $this->session->userdata('nama'),	
				"data_test" => $this->model->GetDataTestEprtSttpln(" and a.id_test = $idTest")->result_array()
			);
	$this->load->view("test/data_test_eprt_sttpln",$dataV);
  }
  
  	function deleteTest($kode = 1){
		$data[]=array(
			"id_test" => $kode,
			"deleted_date" => date("Y-m-d H:i:s"),
		);

		$result = $this->model->Hapus('tb_test_eprt_sttpln', array('id_test' => $kode));

		if($result == true){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'testeprtsttpln');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'testeprtsttpln');
		}
	}
	
	function updateSelectedTest(){

		$data[]=array(
			"id_test" =>  $this->input->post("id_test")==""?null: $this->input->post("id_test"),
			"tanggal_test"  =>  $this->input->post("tanggal_test")==""?null: $this->input->post("tanggal_test"),
			"nim_nik"  =>  $this->input->post("nim_nik")==""?null: $this->input->post("nim_nik"),
			"prodi"  =>  $this->input->post("prodi")==""?null: $this->input->post("prodi"),
			"nama_lengkap"  =>  $this->input->post("nama_lengkap")==""?null: $this->input->post("nama_lengkap"),
			"type_test"  =>  $this->input->post("type_test")==""?null: $this->input->post("type_test"),
			"ruang"  =>  $this->input->post("ruang")==""?null: $this->input->post("ruang"),
			"lc"  =>  $this->input->post("lc")==""?null: $this->input->post("lc"),
			"gc"  =>  $this->input->post("gc")==""?null: $this->input->post("gc"),
			"rc"  =>  $this->input->post("rc")==""?null: $this->input->post("rc"),
			"clc"  =>  $this->input->post("clc")==""?null: $this->input->post("clc"),
			"cgc"  =>  $this->input->post("cgc")==""?null: $this->input->post("cgc"),
			"crc"  =>  $this->input->post("crc")==""?null: $this->input->post("crc"),
			"total"  =>  $this->input->post("total")==""?null: $this->input->post("total"),
			"keterangan"  =>  $this->input->post("keterangan")==""?null: $this->input->post("keterangan"),
		);

		$result = $this->model->UpdateTestEprtSttpln($data[0]);

		if($result == true){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Ubah data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'testeprtsttpln');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Ubah data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'testeprtsttpln');
		}
	}
}
