<?php
			$pdf = new Pdf('L', 'mm', 'A5', true, 'UTF-8', false);
			$pdf->SetTitle('Cetak Sertifikat');
			//$pdf->SetHeaderMargin(30);
			$pdf->SetTopMargin(48);
			$pdf->SetLeftMargin(25);
			$pdf->SetRightMargin(30);
			$pdf->setFooterMargin(20);
			$pdf->SetAutoPageBreak(true);
			$pdf->SetAuthor('Author');
			//$pdf->SetDisplayMode('real', 'default');
			$pdf->setPrintHeader(false);
			$pdf->AddPage("L");			
			$pdf->setPrintFooter(false);

			
			$i=0;
	foreach ($data_sertifikat as $row) 
		{
			$datedta= strtotime($row['tanggal_test']);
			$tanggalTest = date('F d, Y',$datedta);
			$tanggalValid = date('F d, Y',strtotime("+2 year",$datedta));
			$cetakan = '';
			$jenisSertifikat ='SP4';
			$ruang = $row['ruang'];
			
			if($row['cetakan_ke']==null || $row['cetakan_ke']==0)
			{
				$cetakan =1;
			}
			else{
			$cetakan = $cetakan+1;
			}
			$noSertifikat = sprintf("%04d", $row['id_test_ecct']);
            $params['data'] = base64_encode('No. '.$jenisSertifikat.'.'.$cetakan.'.'.$noSertifikat.'/BHS.0/'.date("Y"));
            $params['level'] = 'H';
            $params['size'] = 2;
            $params['savename'] = FCPATH.'tes.png';
            $this->ciqrcode->generate($params);
            $query_data = array(
                'no_sertifikat' => 'No. '.$jenisSertifikat.'.'.$cetakan.'.'.$noSertifikat.'/BHS.0/'.date("Y"),
                'type_test' => 'ECCT SMK Telkom',
                'id_test' => $idTest
            );
            $this->db->insert('tb_cetak_sertifikat', $query_data);
            ini_set('display_errors',1);
			$html='
			<table width=100 border="0">
			<tbody>
			<tr>
			<td style="text-align: center; font-size:1.35em;height: 10px;">To whom it may concern <br><p style="line-height:2px;">This is to certify that</p>
			<p style="margin-top:2px;padding-top:2px;text-align: left;line-height:2px;"><font size="12">No. '.$jenisSertifikat.'.'.$cetakan.'. '.$noSertifikat.'/BHS.0/'.date("Y").'</font></p></td>

			</tr>
			</tbody>
			</table>


			<table width=100 border="0">
			<tbody>
			<tr>
			<td colspan="3">
			&nbsp;&nbsp;&nbsp;
			</td>
			</tr>
			<tr >
			<td style="text-align: center; font-size:1.6em; height: 18px;" colspan="3">&nbsp;<strong>'.$row['nama_lengkap'].' &nbsp; </strong></td>
			</tr>
			<tr>
			<td colspan="4">
			&nbsp;&nbsp;&nbsp;
			</td>
			</tr>
			<tr >
			<td  colspan="3" style="font-size:1.3em">has taken an English Communicative Competence Test (ECCT) at '.$ruang.'.</td>
			</tr>
			<tr >
			<td  height="25" width="40%" style="font-size:1.3em;">The results of the test are as follows: </td>
			<td  height="25"  width="30%"  style="font-size:1.3em;"><strong>Subject</strong></td>
			<td  height="25"  width="10%"  style="font-size:1.3em;text-align: center;"><strong>Score</strong></td>
			<td  height="25"  width="20%"  style="font-size:1.3em;"></td>
			</tr>
			<tr >
			<td></td>
			<td style="text-align: left;"><font size="15">Listening</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['l']).'</strong></font></td>
			<td ></td>
			</tr>
			<tr >
			<td></td>
			<td style="text-align: left;"><font size="15">Reading</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['r']).'</strong></font></td>
			<td ></td>
			</tr>
			<tr >
			<td></td>
			<td style="text-align: left;"><font size="15">Converted Listening</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['lc']).'</strong></font></td>
			<td ></td>
			</tr>
			<tr>
			<td></td>
			<td style="text-align: left;"><font size="15">Converted Reading</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['rc']).'</strong></font></td>
			<td ></td>
			</tr>
			<tr>
			<td></td>
			<td style="text-align: left;"><font size="15"><strong>Total</strong></font></td>
			<td style="text-align: center;"><font size="15"><strong>'.$row['total'].'</strong></font></td>
			<td rowspan="6" align="center"><br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;	<img src="'.base_url().'tes.png"  /></td>
			</tr>
			<tr>
			<td></td>
			<td style="text-align: left;"><font size="15"></font></td>
			<td style="text-align: center;"><font size="15"><strong></strong></font></td>
		
			</tr>
			<tr>
		<td></td>
			<td style="text-align: left;"><font size="15"></font></td>
			<td style="text-align: left;"><font size="15"></font></td>
			</tr>
			<tr>
			<td colspan="2" style="font-size:13"></td>

			</tr>
			<tr >
			<td colspan="2" style="font-size:13">Bandung, '.$tanggalTest.'</td>
			</tr>
			<tr style="height: 40px;">
			    <td style="height: 40px;padding-bottom:2px;" colspan="2" ></td>
			</tr>
			<tr style="height: 40px;">
			    <td style="height: 40px;padding-bottom:2px;padding-left:10" colspan="2" ></td>
			</tr>
			</tbody>
			</table>

			<div style="height: 100px; text-align:center;">Valid until '.$tanggalValid.'</div>';
		}
			$pdf->writeHTML($html, true, false, true, false, '');
			
			$pdf->Output('test_.pdf', 'I');
?>