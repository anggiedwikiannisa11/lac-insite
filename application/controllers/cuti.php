<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cuti extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->helper('currency_format_helper');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url().'backend');
		}
	}

	public function index()
	{
		$idKar = $this->session->userdata('id_kar');
		//echo $idKar;
		//die();
		$year = date("Y");
		$totalCuti =  $this->model->GetCountCutiTahunan(" where id_kar=$idKar and year(tgl_cuti_to)=$year and status=1");
		$totalCuti =  $totalCuti->jml_cuti;
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_karyawan' => $this->model->GetKaryawanJab(" where p.id_kar=$idKar")->result_array(),
			'totalCuti' => $totalCuti,	
		);
//var_dump($totalCuti);die();
		$this->load->view('cuti/data_cuti', $data);
	}

	function savedata(){
		
		$id_kar =  $_POST['id_kar'];
		$tglCutiForm = $_POST['tgl_mulai'];
		$tglCutiTo = $_POST['tgl_selesai'];		
		$jmlCuti = $_POST['jml_kerja'];
		$alamatCuti = $_POST['alamat_cuti'];
		$alasanCuti = $_POST['alasan'];
		$jmHariKerja = $_POST['jml_kerja'];
		$status ='0';
		$jenisCuti = $_POST['jenis'];

		$data = array(	
			'id_kar'=> $id_kar,
			'tgl_cuti_from' => $tglCutiForm .':00',
			'tgl_cuti_to' => $tglCutiTo . ':00',
			'jml_cuti' => $jmlCuti,
			'alamat_cuti' => $alamatCuti,
			'alasan_cuti' => $alasanCuti,
			'tgl_input' => date("Y-m-d H:i:s"),
			'jml_hari_kerja' => $jmHariKerja,
			'status' => $status,
			'jenis_cuti' => $jenisCuti,
			);
		
		$result = $this->model->Simpan('tb_cuti', $data);
		//var_dump($result);die();
		if($result ==1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'cuti');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'cuti');
		}		
	}
	
	function historycuti(){
		$where="";
		$idKar = $this->session->userdata('id_kar');
		$year = date("Y");
		$totalCuti =  $this->model->GetCountCutiTahunan(" where id_kar=$idKar and year(tgl_cuti_to)='$year' and status=1");
		$totalCuti =  $totalCuti->jml_cuti;
		//var_dump($this->session->userdata('level'));die();
		 if( $this->session->userdata('level') !="1" && $this->session->userdata('level') != "2"){			
			 $where = " where a.id_kar=$idKar";
		 }
		 
		 $data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_karyawan' => $this->model->GetCuti($where)->result_array(),
			'totalCuti' => $totalCuti,			
		);
		
		$this->load->view('cuti/history_cuti', $data);
	}
	


	function updatecuti(){
		
		$method = $this->input->post("eksekusi");
		//var_dump($method);die();
		if($method=="approve")
		{
			$data = array(
			'id' => $this->input->post('id_cuti'),
			'status' => '1',
			'tgl_approve' => date('Y-m-d H:i:s'),			
			);
			
			$result = $this->model->UpdateCuti($data);
			if($result>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Cuti Approved</strong></div>");
				header('location:'.base_url().'cuti/historycuti');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Cuti gagal di approve</strong></div>");
				header('location:'.base_url().'cuti/historycuti');
			}
		}
		
		if($method=="reject")
		{
			$data = array(
			'id' => $this->input->post('id_cuti'),
			'status' => '2',
			'tgl_approve' => date('Y-m-d H:i:s'),			
			);
			
			$result = $this->model->UpdateCuti($data);
			if($result>=0){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Cuti Rejected</strong></div>");
				header('location:'.base_url().'cuti/historycuti');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Cuti gagal di reject</strong></div>");
				header('location:'.base_url().'cuti/historycuti');
			}
		}
		
		if($method=="delete")
		{
			
			$result = $this->model->Hapus('tb_cuti', array('id' =>  $this->input->post('id_cuti')));
			if($result == 1){
				$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
				header('location:'.base_url().'cuti/historycuti');
			}else{
				$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
				header('location:'.base_url().'cuti/historycuti');
			}
		}
		
	}
	
	public function viewcuti()
	{
		$tanggalTesEx1 = $this->input->post("tanggalTesEx1");
		$tanggalTesEx2 = $this->input->post("tanggalTesEx2");
		
		$where ="where 1=1 ";
		$count=0;
		
		if($tanggalTesEx1 !="")
		{
			if($count > 0){
			
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tgl_cuti_from) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tgl_cuti_from) = '$tanggalTesEx1'";
				}
			}
			else if($count == 0)
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tgl_cuti_from) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tgl_cuti_from) = '$tanggalTesEx1'";
				}
			}
		
		}
			
		//var_dump($where);die();
		$idKar = $this->session->userdata('id_kar');
		$year = date("Y");
		$totalCuti =  $this->model->GetCountCutiTahunan(" where id_kar=$idKar and year(tgl_cuti_to)='$year' and status=1");
		$totalCuti =  $totalCuti->jml_cuti;
		 if( $this->session->userdata('level') !="1" && $this->session->userdata('level') != "2"){
			 $where .= " and a.id_kar=$idKar";
		 }
		 
		 $data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_karyawan' => $this->model->GetCuti($where)->result_array(),
			'totalCuti' => $totalCuti,			
		);
		
		$this->load->view('cuti/history_cuti', $data);
	}

	function updatekaryawan(){
		if($_FILES['file_upload']['error'] == 0):
			$config = array(
				'upload_path' => './assets/upload',
				'allowed_types' => 'gif|jpg|JPG|png',
				'max_size' => '2048',
				
				);
		$this->load->library('upload', $config);      
		$this->upload->do_upload('file_upload');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];
		else:
			$file_name = $this->input->post('foto');
		endif;
		
		$data = array(
			'id_kar' => $this->input->post('id_kar'),
			'nama_kar' => $this->input->post('nama_kar'),
			'pekerjaan' => $this->input->post('pekerjaan'),
			'nohp' => $this->input->post('nohp'),
			'id_jab' => $this->input->post('id_jab'),
			'nippos' => $this->input->post('nippos'),
			'tgl_input_kar' => $this->input->post('tgl_input_kar'),
			
			'foto' => $file_name,
			
			);
		
		$res = $this->model->UpdateKaryawan($data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'karyawan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'karyawan');
		}
	}

}


// Email: kenduanything23@gmail.com
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */