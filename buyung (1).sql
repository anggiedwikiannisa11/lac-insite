-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2018 at 04:55 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buyung`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_absensi`
--

CREATE TABLE `tb_absensi` (
  `id_abs` int(5) NOT NULL,
  `nippos` int(20) NOT NULL,
  `tanggal` date NOT NULL,
  `kodeabsensi` enum('1','2') NOT NULL,
  `jammasuk` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_absensi`
--

INSERT INTO `tb_absensi` (`id_abs`, `nippos`, `tanggal`, `kodeabsensi`, `jammasuk`) VALUES
(1, 111, '2015-06-18', '1', '06:06:09'),
(2, 111, '2015-06-18', '2', '06:08:28');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `id_jab` int(2) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `tgl_input_jab` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`id_jab`, `jabatan`, `tgl_input_jab`) VALUES
(1, 'Kepala Dinas', '2015-06-18 05:17:35'),
(2, 'Sender', '2015-06-18 03:48:19'),
(3, 'Resepsionis', '2015-06-18 04:30:37'),
(4, 'Instruktur', '2018-07-09 05:51:57'),
(5, 'Admin', '2018-07-09 05:55:45');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jam_kerja_default`
--

CREATE TABLE `tb_jam_kerja_default` (
  `jam_default_id` int(11) NOT NULL,
  `jam_kerja_masuk` time NOT NULL,
  `jam_kerja_keluar` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jam_kerja_default`
--

INSERT INTO `tb_jam_kerja_default` (`jam_default_id`, `jam_kerja_masuk`, `jam_kerja_keluar`) VALUES
(1, '08:00:00', '16:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `id_kar` int(2) NOT NULL,
  `id_jab` int(2) NOT NULL,
  `nippos` int(20) NOT NULL,
  `nama_kar` varchar(50) NOT NULL,
  `pekerjaan` varchar(30) NOT NULL,
  `nohp` int(12) NOT NULL,
  `tgl_input_kar` datetime NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`id_kar`, `id_jab`, `nippos`, `nama_kar`, `pekerjaan`, `nohp`, `tgl_input_kar`, `foto`) VALUES
(2, 2, 20140722, 'asep maman', 'Pengirim', 843834883, '2015-06-18 04:39:35', 'yell2.jpg'),
(3, 4, 1290101, 'Pebby Ardin, S.Pd.', '1', 1, '2018-07-09 05:52:52', ''),
(4, 4, 1085102, 'Astri Hidawati, S.Pd.', '1', 1, '2018-07-09 05:53:12', ''),
(5, 4, 1691103, 'Resti Safaat, S.S.', '1', 1, '2018-07-09 05:53:32', ''),
(6, 4, 1376104, 'Erwin Budiman, S.S', '1', 1, '2018-07-09 05:53:51', ''),
(7, 5, 1582201, 'Jajang Abdul Fattah, S.ST.', '1', 1, '2018-07-09 05:54:09', ''),
(8, 5, 1685202, 'Lena Landriana Fasha, S.Sos.', '1', 1, '2018-07-09 05:54:29', ''),
(9, 4, 1592203, 'Krisma Tira Kharisma, A.Md.', '1', 1, '2018-07-09 05:54:45', ''),
(10, 4, 1786204, 'Cut Aprillia, S.Pd.', '1', 1, '2018-07-09 05:55:08', ''),
(11, 5, 1895205, 'Anggie Dwiki Annisa, A.Md.Kom', '1', 1, '2018-07-09 05:55:29', ''),
(12, 5, 1891206, 'Rizky Radityo Putra, S.T.', '1', 1, '2018-07-09 05:57:24', ''),
(13, 4, 1693105, 'Teti Rahmi, S.Pd.', '1', 1, '2018-07-09 05:57:41', ''),
(14, 4, 1688106, 'Siti Saalimah, S.S.', '1', 1, '2018-07-09 05:57:58', ''),
(15, 4, 1388107, 'Larasati Ayuningsih, S.S., M.Pd.', '1', 1, '2018-07-09 05:58:12', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_login`
--

CREATE TABLE `tb_login` (
  `id_user` int(2) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `pass_user` varchar(200) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `level` enum('1','2','3','4','5','6','7','8') NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_login`
--

INSERT INTO `tb_login` (`id_user`, `nama_user`, `pass_user`, `nama`, `level`, `status`) VALUES
(1, 'admin', 'e9bc16b146a54668a8da6a7a7d0eaed9\r\n', 'Admin LAC', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_presensi`
--

CREATE TABLE `tb_presensi` (
  `presensi_id` int(11) NOT NULL,
  `karyawan_id` varchar(20) NOT NULL,
  `periode` date NOT NULL,
  `tanggal` date NOT NULL,
  `jam_kerja_masuk` datetime DEFAULT NULL,
  `jam_masuk` datetime DEFAULT NULL,
  `jam_kerja_keluar` datetime DEFAULT NULL,
  `jam_keluar` datetime DEFAULT NULL,
  `keterangan` tinytext,
  `tipe` varchar(20) DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `1` int(11) DEFAULT NULL,
  `2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_presensi`
--

INSERT INTO `tb_presensi` (`presensi_id`, `karyawan_id`, `periode`, `tanggal`, `jam_kerja_masuk`, `jam_masuk`, `jam_kerja_keluar`, `jam_keluar`, `keterangan`, `tipe`, `last_modified`, `1`, `2`) VALUES
(514, '1891206', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 13:35:00', '2018-07-12 16:30:00', '2018-07-06 16:59:00', NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(515, '1592203', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 13:37:00', '2018-07-12 16:30:00', '2018-07-06 17:00:00', NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(516, '1582201', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 13:39:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:36', NULL, NULL),
(517, '1895205', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 13:40:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:36', NULL, NULL),
(518, '1685202', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 13:42:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:36', NULL, NULL),
(519, '1691103', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 15:48:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:36', NULL, NULL),
(520, '1786204', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 15:50:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:36', NULL, NULL),
(521, '1388107', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 15:56:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(522, '1688106', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', '2018-07-06 15:58:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(523, '1376104', '2018-07-01', '2018-07-06', '2018-07-12 08:00:00', NULL, '2018-07-12 16:30:00', '2018-07-06 16:47:00', NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(524, '1592203', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', '2018-07-09 07:31:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(525, '1582201', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', '2018-07-09 07:31:00', '2018-07-12 16:30:00', '2018-07-09 16:50:00', NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(526, '1691103', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', '2018-07-09 07:31:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(527, '1376104', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', '2018-07-09 07:33:00', '2018-07-12 16:30:00', '2018-07-09 16:50:00', NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(528, '1891206', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', '2018-07-09 07:40:00', '2018-07-12 16:30:00', '2018-07-09 16:47:00', NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(529, '1895205', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', '2018-07-09 07:42:00', '2018-07-12 16:30:00', '2018-07-09 16:48:00', NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(530, '1685202', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', '2018-07-09 07:46:00', '2018-07-12 16:30:00', '2018-07-09 16:48:00', NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(531, '1786204', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', '2018-07-09 08:03:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:37', NULL, NULL),
(532, '1290101', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', NULL, '2018-07-12 16:30:00', '2018-07-09 16:48:00', NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(533, '1085102', '2018-07-01', '2018-07-09', '2018-07-12 08:00:00', NULL, '2018-07-12 16:30:00', '2018-07-09 16:51:00', NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(534, '1582201', '2018-07-01', '2018-07-10', '2018-07-12 08:00:00', '2018-07-10 07:23:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(535, '1592203', '2018-07-01', '2018-07-10', '2018-07-12 08:00:00', '2018-07-10 07:28:00', '2018-07-12 16:30:00', '2018-07-10 16:37:00', NULL, NULL, '2018-07-12 02:28:39', NULL, NULL),
(536, '1691103', '2018-07-01', '2018-07-10', '2018-07-12 08:00:00', '2018-07-10 07:28:00', '2018-07-12 16:30:00', '2018-07-10 16:36:00', NULL, NULL, '2018-07-12 02:28:39', NULL, NULL),
(537, '1085102', '2018-07-01', '2018-07-10', '2018-07-12 08:00:00', '2018-07-10 07:42:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(538, '1895205', '2018-07-01', '2018-07-10', '2018-07-12 08:00:00', '2018-07-10 07:42:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(539, '1891206', '2018-07-01', '2018-07-10', '2018-07-12 08:00:00', '2018-07-10 07:45:00', '2018-07-12 16:30:00', '2018-07-10 16:38:00', NULL, NULL, '2018-07-12 02:28:39', NULL, NULL),
(540, '1685202', '2018-07-01', '2018-07-10', '2018-07-12 08:00:00', '2018-07-10 07:47:00', '2018-07-12 16:30:00', '2018-07-10 16:36:00', NULL, NULL, '2018-07-12 02:28:39', NULL, NULL),
(541, '1376104', '2018-07-01', '2018-07-10', '2018-07-12 08:00:00', '2018-07-10 07:52:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:38', NULL, NULL),
(542, '1582201', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:16:00', '2018-07-12 16:30:00', '2018-07-11 16:32:00', NULL, NULL, '2018-07-12 02:28:40', NULL, NULL),
(543, '1691103', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:21:00', '2018-07-12 16:30:00', '2018-07-11 16:38:00', NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(544, '1592203', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:21:00', '2018-07-12 16:30:00', '2018-07-11 16:34:00', NULL, NULL, '2018-07-12 02:28:40', NULL, NULL),
(545, '1290101', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:35:00', '2018-07-12 16:30:00', '2018-07-11 16:45:00', NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(546, '1376104', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:40:00', '2018-07-12 16:30:00', '2018-07-11 16:46:00', NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(547, '1891206', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:40:00', '2018-07-12 16:30:00', '2018-07-11 16:38:00', NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(548, '1786204', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:42:00', '2018-07-12 16:30:00', '2018-07-11 16:39:00', NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(549, '1085102', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:47:00', '2018-07-12 16:30:00', '2018-07-11 16:39:00', NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(550, '1685202', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 07:52:00', '2018-07-12 16:30:00', '2018-07-11 16:40:00', NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(551, '1693105', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', '2018-07-11 13:15:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:40', NULL, NULL),
(552, '1895205', '2018-07-01', '2018-07-11', '2018-07-12 08:00:00', NULL, '2018-07-12 16:30:00', '2018-07-11 16:34:00', NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(553, '1582201', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 07:22:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:41', NULL, NULL),
(554, '1691103', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 07:32:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:42', NULL, NULL),
(555, '1592203', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 07:32:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:42', NULL, NULL),
(556, '1786204', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 07:45:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:42', NULL, NULL),
(557, '1290101', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 07:47:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:42', NULL, NULL),
(558, '1891206', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 07:50:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:43', NULL, NULL),
(559, '1085102', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 07:53:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:43', NULL, NULL),
(560, '1685202', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 07:55:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:43', NULL, NULL),
(561, '1376104', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 08:03:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:43', NULL, NULL),
(562, '1895205', '2018-07-01', '2018-07-12', '2018-07-12 08:00:00', '2018-07-12 08:04:00', '2018-07-12 16:30:00', NULL, NULL, NULL, '2018-07-12 02:28:44', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_absensi`
--
ALTER TABLE `tb_absensi`
  ADD PRIMARY KEY (`id_abs`);

--
-- Indexes for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`id_jab`);

--
-- Indexes for table `tb_jam_kerja_default`
--
ALTER TABLE `tb_jam_kerja_default`
  ADD PRIMARY KEY (`jam_default_id`);

--
-- Indexes for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`id_kar`);

--
-- Indexes for table `tb_login`
--
ALTER TABLE `tb_login`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  ADD PRIMARY KEY (`presensi_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_absensi`
--
ALTER TABLE `tb_absensi`
  MODIFY `id_abs` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  MODIFY `id_jab` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_jam_kerja_default`
--
ALTER TABLE `tb_jam_kerja_default`
  MODIFY `jam_default_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  MODIFY `id_kar` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tb_login`
--
ALTER TABLE `tb_login`
  MODIFY `id_user` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  MODIFY `presensi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=563;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
