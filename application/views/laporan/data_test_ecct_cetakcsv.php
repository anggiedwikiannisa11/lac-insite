<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=".$fileName.".xls");?>


<table border = "1">
    <thead>
        <tr>
			<td>Tanggal Tes</td>
            <td>NIM/NIK</td>
			<td>prodi</td>
			<td>Nama</td>
			<td>Tipe</td>
			<td>Ruang</td>
			<td>p</td>
			<td>v</td>
			<td>ga</td>
			<td>fs</td>
			<td>ci</td>
			<td>tc</td>
			<td>total</td>
			<td>average</td>
			<td>id</td>
			<td>No Sertifikat</td>	
        </tr>
    </thead>
    <tbody>
		<?php
		$i=0;
		foreach($data_test as $row){ $i++ ?>
		<tr >
            <td><?php echo $row["tanggal_test"] ?></td>
            <td><?php echo $row["nim_nik"] ?></td> 
			<td><?php echo $row["prodi"] ?></td>
			<td><?php echo $row["nama_lengkap"] ?></td>
			<td><?php echo $row["type_test"] ?></td>
			<td><?php echo $row["ruang"] ?></td>
			<td><?php echo $row["p"] ?></td>
			<td><?php echo $row["v"] ?></td>
			<td><?php echo $row["ga"] ?></td>
			<td><?php echo $row["fs"] ?></td>
			<td><?php echo $row["ci"] ?></td>
			<td><?php echo $row["tc"] ?></td>
			<td><?php echo $row["total"] ?></td>
			<td><?php echo $row["average"] ?></td>
			<td><?php echo $row["no_sertifikat"] ?></td>
			<td><?php echo $row["no_sertifikat_comp"] ?></td>
        </tr>
		<?php
		} ?>
    </tbody>
</table>