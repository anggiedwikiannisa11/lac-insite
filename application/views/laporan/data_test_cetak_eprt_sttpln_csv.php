<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=".$fileName.".xls");?>


<table border = "1">
    <thead>
        <tr>
			<td>Tanggal Tes</td>
            <td>NIM/NIK</td>
			<td>fakultas</td>
			<td>prodi</td>
			<td>Nama</td>
			<td>Tipe</td>
			<td>Ruang</td>
			<td>lc</td>
			<td>gc</td>
			<td>rc</td>
			<td>clc</td>
			<td>cgc</td>
			<td>crc</td>
			<td>total</td>
			<td>Keterangan</td>		
			<td>No Sertifikat</td>
        </tr>
    </thead>
    <tbody>
		<?php
		$i=0;
		foreach($data_test as $row){ $i++ ?>
		<tr >
            <td><?php echo $row["tanggal_test"]?></td>
            <td><?php echo $row["nim_nik"]?></td> 
			<td><?php echo $row["fakultas"]?></td>
			<td><?php echo $row["prodi"]?></td>
			<td><?php echo $row["nama_lengkap"]?></td>
			<td><?php echo $row["type_test"]?></td>
			<td><?php echo $row["ruang"]?></td>
			<td><?php echo $row["lc"]?></td>
			<td><?php echo $row["gc"]?></td>
			<td><?php echo $row["rc"]?></td>
			<td><?php echo $row["clc"]?></td>
			<td><?php echo $row["cgc"]?></td>
			<td><?php echo $row["crc"]?></td>
			<td><?php echo $row["total"]?></td>
		   <?php if($row['keterangan']=="TDK HDR"){ ?>
			<td>Tidak Hadir</td>
		  <?php } ?>
		  <?php if($row['keterangan']=="REG"){ ?>
			<td>Reguler</td>
		  <?php } ?>
		  <?php if($row['keterangan']=="ODS"){ ?>
			<td>ODS</td>
		  <?php } ?>
		  <?php if($row['keterangan']==""|| $row['keterangan']==null){ ?>
			<td>-</td>
		  <?php } ?>
		  <td><?php echo $row["no_sertifikat_comp"]?></td>
        </tr>
		<?php
		} ?>
    </tbody>
</table>