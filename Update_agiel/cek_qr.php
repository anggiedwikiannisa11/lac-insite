<!DOCTYPE html>
<html>
<head>
    <?php $this->load->view('inc/head'); ?>
    <style type="text/css">
        .scanner-laser{
            position: absolute;
            margin: 40px;
            height: 30px;
            width: 30px;
        }
        .laser-leftTop{
            top: 0;
            left: 0;
            border-top: solid red 5px;
            border-left: solid red 5px;
        }
        .laser-leftBottom{
            bottom: 0;
            left: 0;
            border-bottom: solid red 5px;
            border-left: solid red 5px;
        }
        .laser-rightTop{
            top: 0;
            right: 0;
            border-top: solid red 5px;
            border-right: solid red 5px;
        }
        .laser-rightBottom{
            bottom: 0;
            right: 0;
            border-bottom: solid red 5px;
            border-right: solid red 5px;
        }
    </style>
</head>
<body class="skin-blue">
<!-- wrapper di bawah footer -->
<div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php $this->load->view('inc/sidebar'); ?>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <b>DATA TEST EPrT</b>
            </h1>
        </section>
        <section class="content">
            <div class="content" id="QR-Code">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="navbar-form navbar-left">
                            <h4>QR SCANNER</h4>
                        </div>
                        <div class="navbar-form navbar-right">
                            <select class="form-control" id="camera-select"></select>
                            <div class="form-group">
                                <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span></button>
                                <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                                <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr width="100%">
                    <div class="panel-body row ">
                        <div class="col-md-6 text-center">
                            <h4>Scanner</h4>
                            <div class="well" style="position: relative;display: inline-block;">
                                <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
                                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="thumbnail" id="result">
                                <div class="well" style="overflow: hidden;">
                                    <img width="320" height="240" id="scanned-img" src="">
                                </div>
                                <div class="caption">
                                    <h3>Scanned result</h3>
                                    <p id="scanned-QR"></p>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid col-md-12">
                            <div class="col-md-12 form-group text-center">
                                <h4>Check your Serial Code</h4>
                                <form action="<?=base_url()?>laporan/cek_qr/" class="form" method="post">
                                    <input type="text" class="form-control" name="serial" placeholder="Put The Serial Code Here">
                                    <button type="submit" class=" form-control btn btn-success">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <table class="table table-responsive table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>
                                            Certificate Number
                                        </th>
                                        <th>
                                            Personal ID Code
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Faculty
                                        </th>
                                        <th>
                                            Major
                                        </th>
                                        <th>
                                            Test Type
                                        </th>
                                        <th>
                                            Test Date
                                        </th>
                                        <th>
                                            Score
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                    </tr>
                                    </thead>
                                    <?php
                                    if (isset($serial)){
                                        ?>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <?=$serial?>
                                        </td>
                                        <td>
                                            <?=$nim_nik?>
                                        </td>
                                        <td>
                                            <?=$fakultas?>
                                        </td>
                                        <td>
                                            <?=$prodi?>
                                        </td>
                                        <td>
                                            <?=$nama_lengkap?>
                                        </td>
                                        <td>
                                            <?=$type_test?>
                                        </td>
                                        <td>
                                            <?=$tanggal_test?>
                                        </td>
                                        <td>
                                            <?=$total?>
                                        </td>
                                        <td>
                                            Verified <i class="fa fa-check-circle bg-success">
                                        </td>
                                    </tr>
                                    </tbody>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('inc/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/webcodecamjs-master/js/qrcodelib.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/webcodecamjs-master/js/webcodecamjquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/webcodecamjs-master/js/mainjquery.js"></script>
<script>
 var txt = "innerText" in HTMLElement.prototype ? "innerText" : "textContent"; var arg = { resultFunction: function(result) { var aChild = document.createElement('li'); aChild[txt] = result.format + ': ' + result.code; document.querySelector('body').appendChild(aChild); } }; var decoder = new WebCodeCamJS("canvas").init(arg).buildSelectMenu('select', 1); setTimeout(function(){ decoder.play(); }, 500); </script>
</script>
</body>
</html>