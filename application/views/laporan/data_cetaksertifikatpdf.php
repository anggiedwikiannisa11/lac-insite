<?php
			$pdf = new Pdf('L', 'mm', 'A5', true, 'UTF-8', false);
			$pdf->SetTitle('Cetak Sertifikat');
			//$pdf->SetHeaderMargin(30);
			$pdf->SetTopMargin(48);
			$pdf->SetLeftMargin(25);
			$pdf->SetRightMargin(30);
			$pdf->setFooterMargin(20);
			$pdf->SetAutoPageBreak(true);
			$pdf->SetAuthor('Author');
			//$pdf->SetDisplayMode('real', 'default');
			$pdf->setPrintHeader(false);
			$pdf->AddPage("L");			
			$pdf->setPrintFooter(false);

			
			$i=0;
	foreach ($data_sertifikat as $row) 
		{
			$datedta= strtotime($row['tanggal_test']);
			$tanggalTest = date('F d, Y',$datedta);
			$tanggalValid = date('F d, Y',strtotime("+2 year",$datedta));
			$cetakan = '';
			$jenisSertifikat =SERTIFIKAT_EPRT;
			
			if($row['cetakan_ke']==null || $row['cetakan_ke']==0)
			{
				$cetakan =1;
			}
			else{
			$cetakan = $cetakan+1;
			}
			$noSertifikat = sprintf("%04d", $row['no_sertifikat']);
            $params['data'] = base64_encode('No. '.$jenisSertifikat.''.$cetakan.'.'.$noSertifikat.'/BHS.0/'.date("Y"));
            $params['level'] = 'H';
            $params['size'] = 2;
            $params['savename'] = FCPATH.'tes.png';
            $this->ciqrcode->generate($params);
            $query_data = array(
                'no_sertifikat' => 'No. '.$jenisSertifikat.''.$cetakan.'.'.$noSertifikat.'/BHS.0/'.date("Y"),
                'type_test' => 'EPrT',
                'id_test' => $idTest
            );
            $this->db->insert('tb_cetak_sertifikat', $query_data);
			$html='
			<table width=100 border="0">
			<tbody>
			<tr>
			<td style="text-align: center; font-size:1.35em;height: 10px;">To whom it may concern <br><p style="line-height:2px;">This is to certify that</p>
			<p style="margin-top:2px;padding-top:2px;text-align: left;line-height:2px;"><font size="12">No. '.$jenisSertifikat.''.$cetakan.'. '.$noSertifikat.'/BHS.0/'.date("Y").'</font></p></td>

			</tr>
			</tbody>
			</table>


			<table width=100 border="0">
			<tbody>
			<tr >
			<td style="text-align: center; font-size:1.6em; height: 18px;" colspan="3">&nbsp;<strong>'.$row['nama_lengkap'].' &nbsp; </strong></td>
			</tr>
			<tr >
			<td  colspan="3" style="font-size:1.3em">has taken an English Proficiency Test (EPrT) at Language Center Telkom University.</td>
			</tr>
			<tr>
			<td colspan="4">
			&nbsp;&nbsp;&nbsp;
			</td>
			</tr>
			<tr >
			<td  height="25" colspan="3" style="font-size:1.3em;">The results of the test are as follows: </td>
			</tr>
			<tr >
			<td ><font size="15"><strong>Subject</strong></font></td>
			<td style="text-align: center;"><font size="15"><strong>Raw Score</strong></font></td>
			<td  style="text-align: center;"><font size="15"><strong>Converted Score</strong></font></td>
			</tr>
			<tr >
			<td ><font size="15">Listening Competence</font></td>
			<td  style="text-align: center;"><font size="15"><strong>'.$row['lc'].'</strong></font></td>
			<td  style="text-align: center;"><font size="15"><strong>'.$row['clc'].'</strong></font></td>
			</tr>
			<tr >
			<td ><font size="15">Grammar Competence</font></td>
			<td  style="text-align: center;"><font size="15"><strong>'.$row['gc'].'</strong></font></td>
			<td  style="text-align: center;"><font size="15"><strong>'.$row['cgc'].'</strong></font></td>
			</tr>
			<tr>
			<td><font size="15">Reading Competence</font></td>
			<td  style="text-align: center;"><font size="15"><strong>'.$row['rc'].'</strong></font></td>
			<td  style="text-align: center;"><font size="15"><strong><strong>'.$row['crc'].'</strong></strong></font></td>
			</tr>
			<tr>
			<td ><font size="15"><strong>TOTAL</strong></font></td>
			<td>&nbsp;</td>
			<td  style="text-align: center;"><font size="15"><strong>'.$row['total'].'</strong></font></td>
			</tr>
			<tr>
    			<td colspan="3"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
			<tr style="padding-top: 10px">
                <td></td>
                <td></td>
                <td rowspan="2" align="center">	
                
                    <img src="'.base_url().'tes.png"  />
                </td>
			</tr>
			<tr >
			<td colspan="2" style="font-size:13">Bandung, '.$tanggalTest.'</td>
			</tr>
			<tr style="height: 30px;">
			    <td style="height: 30px;padding-bottom:2px;" colspan="2" ></td>
			</tr>
			<tr style="height: 30px;">
			    <td style="height: 30px;padding-bottom:2px;" colspan="2" ></td>
			</tr>
			</tbody>
			</table>

			<div style="height: 80px; text-align:center;">Valid until '.$tanggalValid.'</div>';
		}
			$pdf->writeHTML($html, true, false, true, false, '');
			
			$pdf->Output('test_.pdf', 'I');
?>