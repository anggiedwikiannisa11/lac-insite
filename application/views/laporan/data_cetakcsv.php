<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=".$fileName.".xls");?>


<table border = "1">
    <thead>
        <tr>
			<th align="center">No</th>
			<th align="center">ID Pegawai</th>
			<th align="center">Nama</th>
			<th align="center">Hari Kerja</th>
			<th align="center">Tanggal Kerja</th>
			<th align="center">Mulai Kerja</th>
			<th align="center">Batas Waktu Masuk</th>
			<th align="center">Selesai Kerja</th>
			<th align="center">Jam Pulang Kantor</th>
			<th align="center">Tanda Tangan Pegawai</th>
			<th align="center">Tanda Tangan Atasan Langsung</th>
			<th align="center">Tanda Tangan Manager</th>
        </tr>
    </thead>
    <tbody>
		<?php
		$i=0;
		foreach($data_absensi as $row){ $i++ ?>
		<tr >
            <td align="center" bgcolor="<?php echo $row['warna'] ?>"><?php echo $i?></td>
			<td bgcolor="<?php echo $row['warna'] ?>"><?php echo $row['karyawan_id']?></td>
			<td bgcolor="<?php echo $row['warna'] ?>"><?php echo $row['nama_kar']?></td>	
			<td align="center" bgcolor="<?php echo $row['warna'] ?>"><?php echo date('D', strtotime($row['tanggal']))?></td>
			<td align="center" bgcolor="<?php echo $row['warna'] ?>"><?php echo $row['tanggal']?></td>	
			<td align="center" bgcolor="<?php echo $row['warna'] ?>"><?php echo $row['jam_masuk']?></td>	
			<td align="center" bgcolor="<?php echo $row['warna'] ?>"><?php echo $row['jam_kerja_masuk']?></td>	
			<td align="center" bgcolor="<?php echo $row['warna'] ?>"><?php echo $row['jam_keluar']?></td>	
			<td align="center" bgcolor="<?php echo $row['warna'] ?>"><?php echo $row['jam_kerja_keluar']?></td>	
			<td bgcolor="<?php echo $row['warna'] ?>"></td>	
			<td bgcolor="<?php echo $row['warna'] ?>"></td>	
			<td bgcolor="<?php echo $row['warna'] ?>"></td>	
        </tr>
		<?php
		} ?>
    </tbody>
</table>