<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajar extends CI_Controller {

	public function inputpengajar()
	{
		$this->load->view('scheduling/form_pengajar');
	}
	public function inputdata()
	{
		$this->load->model('pengajarmodel', 'pengajar');
		$data = $this->input->post();
		$query = $this->pengajar->saveInputan($data);
		if ($query) {
			echo "<script>alert('Terimakasih, Telah Mengisi Datanya, Semoga ngga pusing ya ^o^', 'refresh')</script>";
			redirect("pengajar/inputpengajar");
		} else {
			echo "<script>alert('masih ada yang kosong tuh')</script>";
		}
	}
	public function showschedule()
	{
		$this->load->view('scheduling/c_pengajar');
	}
	function index(){
		$data['user'] = $this->pengajarmodel->tampil_data()->result();
		$this->load->view('scheduling/c_pengajar',$data);
	}
}