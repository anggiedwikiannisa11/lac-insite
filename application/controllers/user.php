<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->helper('currency_format_helper');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url().'backend');
		}
	}

	public function index()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_user' => $this->model->GetDataUser("order by nama_user asc")->result_array()
		);

		$this->load->view('user/data_user', $data);
	}

	function adduser()
	{
		$data = array(
			'nama' => $this->session->userdata('nama'),	
		);
		
		$this->load->view('user/add_user', $data);
	}

	function savedata(){

		$userName = $_POST['username'];
		$nama = $_POST['nama'];		
		$password = $_POST['password'];
		$level = $_POST['level'];
		$status = $_POST['status'];
		
		$data = array(	
			'nama_user'=> $userName,
			'pass_user' => md5($password),
			'nama' => $nama,
			'level' => $level,
			'status' => $status
		);
		
		$result = $this->model->Simpan('tb_login', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Simpan data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'user');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Simpan data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'user');
		}		
	}

	function edituser($id = 0){
		$data_user = $this->model->GetDataUser("where id_user = $id")->result_array();

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'id_user' => $data_user[0]['id_user'],
			'nama_user' => $data_user[0]['nama_user'],
			'nama' => $data_user[0]['nama'],
			'password' => $data_user[0]['pass_user'],
			'status' => $data_user[0]['status'],
			'level' => $data_user[0]['level']
			);
			
		$this->load->view('user/edit_user', $data);
	}

	function hapuskar($kode = 1){
		
		$result = $this->model->Hapus('tb_karyawan', array('id_kar' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'karyawan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'karyawan');
		}
	}

	function updateuser(){

		$password = $this->input->post('password');
		if($password != "")
		{
			$data = array(	
				'id_user' => $this->input->post('iduser'),
				'nama_user'=> $this->input->post('username'),
				'pass_user' => md5($password),
				'nama' => $this->input->post('nama'),
				'level' => $this->input->post('level'),
				'status' => $this->input->post('status')
			);
		}
		
		else
		{
			$data = array(	
				'id_user' => $this->input->post('iduser'),
				'nama_user'=> $this->input->post('username'),
				'nama' => $this->input->post('nama'),
				'level' => $this->input->post('level'),
				'status' => $this->input->post('status')
			);
		}
		
		$res = $this->model->UpdateUser($data);
		if($res>=0){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Update data BERHASIL di lakukan</strong></div>");
			header('location:'.base_url().'user');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Update data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'user');
		}
	}
	
	function aktifkanuser(){

		$idKaryawan = $this->uri->segment(3);
		//$nama = $this->uri->segment(4);
		//$nip = $this->uri->segment(5);
		$data_karyawan = $this->model->GetKaryawan("where id_kar = '$idKaryawan'")->first_row();
		$data = array(	
			'nama_user'=> $data_karyawan->nippos,
			'id_kar'=> $idKaryawan,
			'pass_user' => md5($data_karyawan->nippos),
			'nama' => $data_karyawan->nama_kar,
			'level' => '6',
			'status' => '1'
		);
		
		$result = $this->model->Simpan('tb_login', $data);
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>User diaktfkan</strong></div>");
			header('location:'.base_url().'karyawan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>User Gagal Diaktifkan</strong></div>");
			header('location:'.base_url().'karyawan');
		}		
	}
	
	function nonaktifkanuser($kode = 1){
		
		$result = $this->model->Hapus('tb_login', array('id_kar' => $kode));
		if($result == 1){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'karyawan');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'karyawan');
		}
	}

}


// Email: kenduanything23@gmail.com
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */