<?php
			$pdf = new Pdf('L', 'mm', 'A5', true, 'UTF-8', false);
			$pdf->SetTitle('Cetak Sertifikat');
			//$pdf->SetHeaderMargin(30);
			$pdf->SetTopMargin(40);
			$pdf->SetLeftMargin(25);
			$pdf->SetRightMargin(30);
			$pdf->setFooterMargin(20);
			$pdf->SetAutoPageBreak(true);
			$pdf->SetAuthor('Author');
			//$pdf->SetDisplayMode('real', 'default');
			$pdf->setPrintHeader(false);
			$pdf->AddPage("L");			
			$pdf->setPrintFooter(false);

			
			$i=0;
	foreach ($data_sertifikat as $row) 
		{
			$datedta= strtotime($row['tanggal_test']);
			$tanggalTest = date('F d, Y',$datedta);
			$tanggalValid = date('F d, Y',strtotime("+2 year",$datedta));
			$cetakan = '';
			$jenisSertifikat =SERTIFIKAT_ECCT;
			
			if($row['cetakan_ke']==null || $row['cetakan_ke']==0)
			{
				$cetakan =1;
			}
			else{
			$cetakan = $cetakan+1;
			}
			$noSertifikat = sprintf("%04d", $row['no_sertifikat']);
            $params['data'] = base64_encode('No. '.$jenisSertifikat.''.$cetakan.'.'.$noSertifikat.'/BHS.0/'.date("Y"));
            $params['level'] = 'H';
            $params['size'] = 2;
            $params['savename'] = FCPATH.'tes.png';
            $this->ciqrcode->generate($params);
            $query_data = array(
                'no_sertifikat' => 'No. '.$jenisSertifikat.''.$cetakan.'.'.$noSertifikat.'/BHS.0/'.date("Y"),
                'type_test' => 'ECCT',
                'id_test' => $idTest
            );
            $this->db->insert('tb_cetak_sertifikat', $query_data);
            ini_set('display_errors',1);
			$html='
			<table width=100 border="0">
			<tbody>
			<tr>
			<td style="text-align: center; font-size:1.35em;height: 10px;">To whom it may concern <br><p style="line-height:2px;">This is to certify that</p>
			<p style="margin-top:2px;padding-top:2px;text-align: left;line-height:2px;"><font size="12">No. '.$jenisSertifikat.''.$cetakan.'. '.$noSertifikat.'/BHS.0/'.date("Y").'</font></p></td>

			</tr>
			</tbody>
			</table>


			<table width=100 border="0">
			<tbody>
			<tr >
			<td style="text-align: center; font-size:1.6em; height: 18px;" colspan="3">&nbsp;<strong>'.$row['nama_lengkap'].' &nbsp; </strong></td>
			</tr>
			<tr >
			<td  colspan="3" style="font-size:1.3em">has taken an English Communicative Competence Test (ECCT) at Language Center Telkom University.</td>
			</tr>
			<tr >
			<td  height="25" width="40%" style="font-size:1.3em;">The results of the test are as follows: </td>
			<td  height="25"  width="30%"  style="font-size:1.3em;"><strong>Subject</strong></td>
			<td  height="25"  width="10%"  style="font-size:1.3em;text-align: center;"><strong>Score</strong></td>
			<td  height="25"  width="20%"  style="font-size:1.3em;"></td>
			</tr>
			<tr >
			<td></td>
			<td style="text-align: left;"><font size="15">Pronunciation</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['p']).'</strong></font></td>
			<td ></td>
			</tr>
			<tr >
			<td></td>
			<td style="text-align: left;"><font size="15">Vocabulary Use</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['v']).'</strong></font></td>
			<td ></td>
			</tr>
			<tr >
			<td></td>
			<td style="text-align: left;"><font size="15">Grammar Accuracy</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['ga']).'</strong></font></td>
			<td ></td>
			</tr>
			<tr>
			<td></td>
			<td style="text-align: left;"><font size="15">Flow of Speech</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['fs']).'</strong></font></td>
			<td ></td>
			</tr>
			<tr>
			<td></td>
			<td style="text-align: left;"><font size="15">Construction of Idea</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['ci']).'</strong></font></td>
			<td rowspan="6" align="center"><br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;	<img src="'.base_url().'tes.png"  /></td>
			</tr>
			<tr>
			<td></td>
			<td style="text-align: left;"><font size="15">Task Completion</font></td>
			<td style="text-align: center;"><font size="15"><strong>'.floatval($row['tc']).'</strong></font></td>
		
			</tr>
			<tr>
		<td></td>
			<td style="text-align: left;"><font size="15"><strong>AVERAGE</strong></font></td>
			<td style="text-align: left;"><font size="15">&nbsp;&nbsp;&nbsp;<strong>'.$row['average'].'</strong></font></td>
			</tr>
			<tr>
			<td colspan="2" style="font-size:13"></td>

			</tr>
			<tr >
			<td colspan="2" style="font-size:13">Bandung, '.$tanggalTest.'</td>
			</tr>
			<tr style="height: 60px;">
			    <td style="height: 60px;padding-bottom:2px;" colspan="2" ></td>
			</tr>
			<tr style="height: 50px;">
			    <td style="height: 50px;padding-bottom:2px;" colspan="2" ></td>
			</tr>
			</tbody>
			</table>

			<div style="height: 100px; text-align:center;">Valid until '.$tanggalValid.'</div>';
		}
			$pdf->writeHTML($html, true, false, true, false, '');
			
			$pdf->Output('test_.pdf', 'I');
?>