<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=data_cuti_".date('Y-m-d His').".xls");?>


<table border = "1">
    <thead>
        <tr>
			<td>No</td>
            <td>NIP</td>
			<td>Nama</td>
			<td>Tanggal mulai cuti</td>
			<td>Tanggal selesai cuti</td>
			<td>Jumlah hari kerja</td>
			<td>Alamat cuti</td>
			<td>Alasan cuti</td>
			<td>Jenis Keperluan</td>
			<td>Status</td>
			<td>Tgl Approve</td>
        </tr>
    </thead>
    <tbody>
		<?php
		$i=0;
		foreach($data_karyawan as $row){ $i++ ?>
		<tr >
            <td><?php echo $i; ?></td>
            <td><?php echo $row["nippos"]?></td> 
			<td><?php echo $row["nama_kar"]?></td>
			<td><?php echo $row["tgl_cuti_from"]?></td>
			<td><?php echo $row["tgl_cuti_to"]?></td>
			<td><?php echo $row["jml_hari_kerja"]?></td>
			<td><?php echo $row["alamat_cuti"]?></td>
			<td><?php echo $row["alasan_cuti"]?></td>
			<td><?php echo $row["jenis_cuti"]?></td>			
		   <?php if($row['status']=="0"){ ?>
			<td>Waiting</td>
		  <?php } else if($row['status']=="1"){ ?>
			<td>Approved</td>
		  <?php } else if($row['status']=="2"){ ?>
			<td>Rejected</td>
		  <?php } else { ?>
			<td>-</td>
		  <?php } ?>
		  <td><?php echo $row["tgl_approve"]?></td>
        </tr>
		<?php
		} ?>
    </tbody>
</table>