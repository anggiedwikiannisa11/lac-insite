<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testecct extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->helper('currency_format_helper');
		$this->load->library('excel');
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url().'backend');
		}
	}

	public function index()
	{

		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_test' => array()//$this->model->GetDataTestEcct("")->result_array(),
		);

		$this->load->view('test/data_test_ecct', $data);
	}
	
	public function viewTest()
	{
		$tipeTes = $this->input->post("tipeTes");
		$tanggalTesEx1 = $this->input->post("tanggalTesEx1");
		$tanggalTesEx2 = $this->input->post("tanggalTesEx2");
		$waktuTes = $this->input->post("waktuTes");
		//$keterangan = $this->input->post("keterangan");
		$nim_nama = $this->input->post("nim_nama");
		
		$where ="";
		$count=0;
		
		if($tipeTes !="")
		{
			$where .=" and type_test = '$tipeTes'";
			$count++;
		}		
		
		if($tanggalTesEx1 !="")
		{
			if($count > 0  && $waktuTes == ""){
			
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tanggal_test) = '$tanggalTesEx1'";
				}
			}
			else if($count == 0  && $waktuTes == "")
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tanggal_test) = '$tanggalTesEx1'";
				}
			}
			
			elseif($count > 0  && $waktuTes != ""){

				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' and Time(tanggal_test) = '$waktuTes' ";
				}
				else
				{
					$tanggalTestAll = $tanggalTes." ".$waktuTes;
					$where .=" and tanggal_test = '$tanggalTestAll'";
				}
			}
			else if($count == 0  && $waktuTes != "")
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' and Time(tanggal_test) = '$waktuTes' ";
				}
				else
				{
					$tanggalTestAll = $tanggalTes." ".$waktuTes;
					$where .=" and tanggal_test = '$tanggalTestAll'";
				
				}
			}
		}
		
		// if($keterangan != "")
		// {
			// if($count > 0)
			// {
				// $where .= "and keterangan = '$keterangan'";
			// }
			// else
			// {
				// $where .= "and keterangan = '$keterangan'";
			// }
		// }
		
		if($nim_nama != "")
		{
			if($count > 0)
			{
				$where .= "and nim_nik like '%$nim_nama%' or nama_lengkap like '%$nim_nama%'";
			}
			else
			{
				$where .= "and nim_nik like '%$nim_nama%' or nama_lengkap like '%$nim_nama%'";
			}
		}

		//var_dump($where);die();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_test' => $this->model->GetDataTestEcct($where)->result_array(),
		);

		$this->load->view('test/data_test_ecct', $data);
	}
	
	public function import()
	{
	  if(isset($_FILES["file"]["name"]))
	  {
	   $path = $_FILES["file"]["tmp_name"];
	   $object = PHPExcel_IOFactory::load($path);
	   //$object->setReadDataOnly(true);
	   	$noser=$this->model->GetLastNoCertificateEcct();
		//var_dump($noser);die();
		$noSertifikat =0;
		$pos = 0;
	   foreach($object->getWorksheetIterator() as $worksheet)
	   {
		$highestRow = $worksheet->getHighestRow();
		$highestColumn = $worksheet->getHighestColumn();
		
		$rowTipeTes = $worksheet->getCellByColumnAndRow(1, 1)->getValue();
		$rowTanggalTes = $worksheet->getCellByColumnAndRow(1, 2)->getValue();
		$rowWaktuTes = $worksheet->getCellByColumnAndRow(1, 3)->getValue();
		$rowRuangTes = $worksheet->getCellByColumnAndRow(1, 4)->getValue();
		
		$rowTipeTesExp = explode(":",$rowTipeTes );
		$rowTanggalTesExp = explode(":",$rowTanggalTes );
		$rowWaktuTesExp = explode(":",$rowWaktuTes );
		$rowRuangTesExp = explode(":",$rowRuangTes );
		
		$rowTanggalTesExp = explode(" ",$rowTanggalTesExp[1]);
		$montNum = $this->model->GetIndonesianDateNameToNumber($rowTanggalTesExp[2]);
		
		
		//var_dump($pos,$noSertifikat);
		
		$tipeTes_ = trim($rowTipeTesExp[1]);		
		$waktuTes_ =$rowWaktuTesExp[1].":".$rowWaktuTesExp[2];
		$tanggalTes_ =$rowTanggalTesExp[3]."-".$montNum."-".$rowTanggalTesExp[1]." ".$waktuTes_;
		$ruangTes_ = trim($rowRuangTesExp[1]);
		$userId = $this->session->userdata('id_user');
		//var_dump($highestRow);die();
		for($row=7; $row<=$highestRow; $row++)
		{
		 $nim_nik = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
		 $nama = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
		 $prodi = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
		 $p = $worksheet->getCellByColumnAndRow(5, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(5, $row)->getValue();
		 $v = $worksheet->getCellByColumnAndRow(6, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(6, $row)->getValue();
		 $ga = $worksheet->getCellByColumnAndRow(7, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(7, $row)->getValue();
		 $fs = $worksheet->getCellByColumnAndRow(8, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(8, $row)->getValue();
		 $ci = $worksheet->getCellByColumnAndRow(9, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(9, $row)->getValue();
		 $tc = $worksheet->getCellByColumnAndRow(10, $row)->getValue()=="-"?null:$worksheet->getCellByColumnAndRow(10, $row)->getValue();
		 $total = $worksheet->getCellByColumnAndRow(11, $row)->getCalculatedValue()=="-"?null:$worksheet->getCellByColumnAndRow(11, $row)->getCalculatedValue();
		 $average = $worksheet->getCellByColumnAndRow(12, $row)->getCalculatedValue()=="-"?null:$worksheet->getCellByColumnAndRow(12, $row)->getCalculatedValue();
		 
		 if($noser->noser==null && $noSertifikat ==0)
		{			
			$noSertifikat =SERTIFIKAT_ECCT_NO;	
		}
		else if($pos==0 &&$noser->noser != null)
		{
			$noSertifikat =$noser->noser;
			$noSertifikat = $noSertifikat +1;
			$pos++;
			//var_dump($pos);die();
		}
		else 
		{
			$noSertifikat = $noSertifikat +1;
		}
		
		 $data[] = array(
		  'no_sertifikat'  => $noSertifikat,
		  'tanggal_test'  => $tanggalTes_,
		  'nim_nik'   => $nim_nik,
		  'prodi'  => $prodi,
		  'nama_lengkap'  => $nama,
		  'type_test'  => $tipeTes_,
		  'ruang'  => $ruangTes_,
		  'p'  => str_replace(",",".",$p),
		  'v'  => str_replace(",",".",$v),
		  'ga'  => str_replace(",",".",$ga),
		  'fs'  => str_replace(",",".",$fs),
		  'ci'  => str_replace(",",".",$ci),
		  'tc'  => str_replace(",",".",$tc),
		  'total'  => str_replace(",",".",$total),
		  'average'  => str_replace(",",".",$average),
		  'user_modified'=>$userId
		 );
		}
	   }
	   $this->model->Insert("tb_test_ecct",$data);
	  // echo 'Data Imported successfully';
	}


 }
public function get_test_detail()
 {
        $phoneData = $this->input->post('phoneData');
		
        if(isset($phoneData) and !empty($phoneData)){
            $records = $this->model->GetDataTestEcct(" and nim_nik ='$phoneData'")->result_array();
			//var_dump($records);die();
            $output = ' 
         <div class="col-lg-12">
          <table class="table table-bordered">
           <tr>
            <td>Tanggal Tes</td>
            <td>NIM/NIK</td>
			<td>prodi</td>
			<td>Nama</td>
			<td>Tipe</td>
			<td>Ruang</td>
			<td>p</td>
			<td>v</td>
			<td>ga</td>
			<td>fs</td>
			<td>ci</td>
			<td>tc</td>
			<td>total</td>
			<td>average</td>
			<td>id</td>
			<td>No Sertifikat</td>			
           </tr>';
            foreach($records as $row){
             $output .= '      
           <tr>
            <td>'.$row["tanggal_test"].'</td>
            <td>'.$row["nim_nik"].'</td> 
			<td>'.$row["prodi"].'</td>
			<td>'.$row["nama_lengkap"].'</td>
			<td>'.$row["type_test"].'</td>
			<td>'.$row["ruang"].'</td>
			<td>'.$row["p"].'</td>
			<td>'.$row["v"].'</td>
			<td>'.$row["ga"].'</td>
			<td>'.$row["fs"].'</td>
			<td>'.$row["ci"].'</td>
			<td>'.$row["tc"].'</td>
			<td>'.$row["total"].'</td>
			<td>'.$row["average"].'</td>
			<td>'.$row["no_sertifikat"].'</td>
			<td>'.$row["no_sertifikat_comp"].'</td>
           </tr>                           

	  ';
            }
			$output .='</table>
					</div>';
            echo $output;
        }
        else {
         echo '<center><ul class="list-group"><li class="list-group-item">'.'Select a Phone'.'</li></ul></center>';
        }
	}

  public function validasiSertifikat()
  {
	$idTest = $this->uri->segment(3);
	

	$dataSertifikat = $this->model->GetDataTestEcct(" and a.id_test_ecct=$idTest")->first_row();
	$jenisSertifikat =SERTIFIKAT_ECCT;
	if($dataSertifikat->cetakan_ke ==null || $dataSertifikat->cetakan_ke==0)
	{
		$cetakan =1;
	}
	else
	{
		$cetakan = $dataSertifikat->cetakan_ke;
		$cetakan = $cetakan+1;
	}
	$noSertifikat = sprintf("%04d", $dataSertifikat->no_sertifikat);
	$noSertifikatComp = $jenisSertifikat."".$cetakan.". ".$noSertifikat."/BHS.0/".date("Y");
	$data[]=array(
			"cetakan_ke" => $cetakan,
			"id_test_ecct"=> $idTest,
			"no_sertifikat_comp" => $noSertifikatComp
			);
	$this->model->UpdateTestEcct($data[0]);
	
	$dataV = array
			(
			'nama' => $this->session->userdata('nama'),	
				"data_test" => $this->model->getDataTestEcct(" and a.id_test_ecct = $idTest")->result_array()
			);
	$this->load->view("test/data_test_ecct",$dataV);
  }
  
  	function deleteTest($kode = 1){
		$data[]=array(
			"id_test_ecct" => $kode,
			"deleted_date" => date("Y-m-d H:i:s"),
		);
		//var_dump($data);die();
		//$result = $this->model->UpdateTestEcct($data[0]);
		$result = $this->model->Hapus('tb_test_ecct', array('id_test_ecct' => $kode));
		//var_dump($result);die();
		if($result == true){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Hapus data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'testecct');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Hapus data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'testecct');
		}
	}
	
	function updateSelectedTest(){
		
		
		$data[]=array(
			"id_test_ecct" =>  $this->input->post("id_test_ecct")==""?null: $this->input->post("id_test_ecct"),
			"tanggal_test"  =>  $this->input->post("tanggal_test")==""?null: $this->input->post("tanggal_test"),
			"nim_nik"  =>  $this->input->post("nim_nik")==""?null: $this->input->post("nim_nik"),
			"prodi"  =>  $this->input->post("prodi")==""?null: $this->input->post("prodi"),
			"nama_lengkap"  =>  $this->input->post("nama_lengkap")==""?null: $this->input->post("nama_lengkap"),
			"type_test"  =>  $this->input->post("type_test")==""?null: $this->input->post("type_test"),
			"ruang"  =>  $this->input->post("ruang")==""?null: $this->input->post("ruang"),
			"p"  =>  $this->input->post("p")==""?null: $this->input->post("p"),
			"v"  =>  $this->input->post("v")==""?null: $this->input->post("v"),
			"ga"  =>  $this->input->post("ga")==""?null: $this->input->post("ga"),
			"fs"  =>  $this->input->post("fs")==""?null: $this->input->post("fs"),
			"ci"  =>  $this->input->post("ci")==""?null: $this->input->post("ci"),
			"tc"  =>  $this->input->post("tc")==""?null: $this->input->post("tc"),
			"total"  =>  $this->input->post("total")==""?null: $this->input->post("total"),
			"average"  =>  $this->input->post("average")==""?null: $this->input->post("average"),
		);
		//var_dump($data);die();
		$result = $this->model->UpdateTestEcct($data[0]);
		//$result = $this->model->Hapus('tb_test_ecct', array('id_test_ecct' => $kode));
		//var_dump($result);die();
		if($result == true){
			$this->session->set_flashdata("sukses", "<div class='alert alert-success'><strong>Ubah data BERHASIL dilakukan</strong></div>");
			header('location:'.base_url().'testecct');
		}else{
			$this->session->set_flashdata("alert", "<div class='alert alert-danger'><strong>Ubah data GAGAL di lakukan</strong></div>");
			header('location:'.base_url().'testecct');
		}
	}
}
