<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>DATA TEST EPrT ITTP</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
			
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
		 <?php if($this->session->userdata('level') ==1){ ?>
		  <div class="col-md-12">
		
		  	<div class="box">
				 <form method="post" id="import_form" enctype="multipart/form-data">

				   <p style="float:left;"><label>Select Excel File</label> &nbsp</p>
				   <input  type="file" style="float:left;" name="file" id="file" required accept=".xls, .xlsx" />

				   <input type="submit"   name="import" value="Import" class="" />

				  </form>
				</div>
		   </div>
		  <?php } ?>
		  
		   <?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==3){ ?>
		  <div class="col-md-12">
				<div class="box">
				<h4><strong>Export</strong></h4>
					<form role="form" action="<?php echo base_url(); ?>laporan/cetak_test_eprt_ittp_csv" method="POST">
					<table  class="table table-bordered table-striped">
						<tr>
							<td>
								<select class="form-control" style="width:200px" name="tipeTes">
									<option value="">--Tipe Tes--</option>
									<option value="EPrT">EPrT</option>
									<option value="EPrT Online">EPrT Online</option>
									<option value="EPrT Calus (Online)">EPrT Calus (Online)</option>
									<option value="EPrT Post Test">EPrT Post Test</option>
								</select>
							</td>
							<td>

									<input class="form-control" id="datetimepicker4" type="text" placeholder="Tanggal Tes 1" name ="tanggalTesEx1">
							
							</td>
							<td>

									-
							
							</td>
							<td>

									<input class="form-control" id="datetimepicker5" type="text" placeholder="Tanggal Tes 2" name ="tanggalTesEx2">
							
							</td>

							<td>
								<input type="text" id="timepicker1" class="form-control" name="waktuTes" placeholder="Waktu Tes" style="width:100px">
							</td>
							<td>
								<select class="form-control" style="width:150px" name="keterangan">
									<option value="">--Keterangan--</option>
									<option value="TDK HDR">Tidak Hadir</option>
									<option value="REG">Reguler</option>
									<option value="ODS">ODS</option>
								</select>
							</td>
							<td>
							<button type="submit" class="btn btn-warning">export</button>
							</td>
						</tr>
					</table>
				</form>
				</div>
		   </div>
		  <?php } ?>
		  
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
				
                </div><!-- /.box-title -->
                <div class="box-body">
				<form role="form" action="<?php echo base_url(); ?>testeprtittp/viewTest" method="POST">
					<table  class="table table-bordered table-striped">
						<tr>
							<td>
								<select class="form-control" style="width:200px" name="tipeTes">
									<option value="">--Tipe Tes--</option>
									<option value="EPrT">EPrT</option>
									<option value="EPrT Online">EPrT Online</option>
									<option value="EPrT Calus (Online)">EPrT Calus (Online)</option>
									<option value="EPrT Post Test">EPrT Post Test</option>
								</select>
							</td>
							<td>
									<input class="form-control" id="datetimepicker6" type="text" placeholder="Tanggal Tes 1" name ="tanggalTesEx1"></input>

							</td>
							<td>
									-
							</td>
							<td>
									<input class="form-control" id="datetimepicker7" type="text" placeholder="Tanggal Tes 2" name ="tanggalTesEx2"></input>
							</td>

							<td>
								<input type="text" id="timepicker2" class="form-control" name="waktuTes" placeholder="Waktu Tes" style="width:100px">
							</td>

							<td>
								<select class="form-control" style="width:150px" name="keterangan">
									<option value="">--Keterangan--</option>
									<option value="TDK HDR">Tidak Hadir</option>
									<option value="REG">Reguler</option>
									<option value="ODS">ODS</option>
								</select>
							</td>
							<td>
								<input type="text" class="form-control" name="nim_nama" placeholder="NIM/NAMA" style="width:100px">
							</td>
							<td>
							<button type="submit" class="btn btn-success">Lihat Data</button>
							</td>
						</tr>

						<tr >

						</tr>
					</table>
				</form>
				
				<br>
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
					<th>No</th>
                      <th>Tanggal Tes</th>
                      <th>Tipe Tes</th>
                      <th>Nama</th>
                      <th>Id Peserta</th>
                      <th>Ruangan</th>
                      <th>Nilai Total</th>
					  <th>Keterangan</th>
					  <?php if( $this->session->userdata('level') !=4){ ?>
                      <th>Aksi</th>
					  <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_test as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row['tanggal_test']; ?></td>
                      <td><?php echo $row['type_test']; ?></td>
                      <td><?php echo $row['nama_lengkap']; ?></td>
                      <td><?php echo $row['nim_nik']; ?></td>
					  <td><?php echo $row['ruang']; ?></td>
                      <td><?php echo $row['total']; ?></td>
					  <?php if($row['keterangan']=="TDK HDR"){ ?>
						<td>Tidak Hadir</td>
					  <?php } else if($row['keterangan']=="REG"){ ?>
						<td>Reguler</td>
					  <?php } else  if($row['keterangan']=="ODS"){ ?>
						<td>ODS</td>
					  <?php } else if($row['keterangan']==""|| $row['keterangan']==null){ ?>
						<td>-</td>
					  <?php } else { ?>
					  <td><?php echo $row['keterangan']; ?></td>
					  <?php } ?>
					  <?php if( $this->session->userdata('level') !=4){ ?>
					  <td>
                     
					   <a  target="_blank" href="<?php echo base_url(); ?>laporan/cetak_sertifikat_eprt_ittp_pdf/<?php echo $row['id_test'] ?> "><img style="width:25px;height:25px" src="<?php echo base_url(); ?>assets/upload/cetak_sertifikat.png"  /></a> &nbsp;
					   <a  href ="javascript:void(0)" class="view_data" value="detail" id="<?php echo $row['nim_nik']; ?> "><img style="width:25px;height:25px" src="<?php echo base_url(); ?>assets/upload/details_peserta.png"  /></a> 
					   <a class="btn btn-warning" href="<?php echo base_url(); ?>testeprtittp/validasiSertifikat/<?php echo $row['id_test']; ?>"><?php echo $row['cetakan_ke'] + 1 ?></a>
					   <?php if($this->session->userdata('level') ==1){ ?>
					   <a onclick="return confirm('Hapus data??');" class="btn btn-danger btn-sm" href="<?php echo base_url(); ?>testeprtittp/deleteTest/<?php echo $row['id_test']; ?>"><i class="fa fa-trash"></i></a>
					   <a 
								href="javascript:;"
								data-id_test="<?php echo  $row['id_test']; ?>"
								data-tanggal_test="<?php echo  $row['tanggal_test']; ?>"
								data-nama_lengkap="<?php echo  $row['nama_lengkap']; ?>"
								data-nim_nik="<?php echo  $row['nim_nik']; ?>"
								data-ruang="<?php echo  $row['ruang']; ?>"
								data-prodi="<?php echo  $row['prodi']; ?>"
								data-type_test="<?php echo  $row['type_test']; ?>"
								data-lc="<?php echo  $row['lc']; ?>"
								data-gc="<?php echo  $row['gc']; ?>"
								data-rc="<?php echo  $row['rc']; ?>"
								data-clc="<?php echo  $row['clc']; ?>"
								data-cgc="<?php echo  $row['cgc']; ?>"
								data-crc="<?php echo  $row['crc']; ?>"
								data-total="<?php echo  $row['total']; ?>"
								data-keterangan="<?php echo  $row['keterangan']; ?>"
								data-toggle="modal" data-target="#edit-data">
                            <button  data-toggle="modal" data-target="#ubah-data" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"></i></button></a>
							</a>
					   <?php } ?>
                      </td>
					   <?php } ?>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>Copyright &copy; 2018 <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  
<!-- view Modal -->
 <div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="margin-top: -20px;">
   <div class="modal-dialog modal-lg">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 class="modal-title" id="myModalLabel">Test Details</h4>
       </div>
       <div class="modal-body">
        <!-- Place to print the fetched phone -->
         <div id="phone_result"></div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>
   </div>
 </div>
<!-- end of modal -->

<!-- Modal Ubah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Ubah Data</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url('testeprtittp/updateSelectedTest')?>" method="post" enctype="multipart/form-data" role="form">
	            <div class="modal-body">
	                    <div class="form-group">
	                        <div class="col-lg-12">
	                        	<input type="hidden" id="id_test" name="id_test">
								 <table class="table table-bordered">
								   <tr>
									<td>Tanggal Tes</td>
									<td>NIM/NIK</td>
									<td>prodi</td>
									<td>Nama</td>
									<td>Tipe</td>
									<td>Ruang</td>
									<td>lc</td>
									<td>gc</td>
									<td>rc</td>
									<td>clc</td>
									<td>cgc</td>
									<td>crc</td>
									<td>total</td>
									<td>keterangan</td>	
								   </tr>     
								   <tr>
									<td><input type="text" id="tanggal_test" name="tanggal_test" size="5"></td>
									<td><input type="text" id="nim_nik" name="nim_nik" size="3"></td> 
									<td><input type="text" id="prodi" name="prodi" size="2"></td>
									<td><input type="text" id="nama_lengkap" name="nama_lengkap" size="6"></td>
									<td><input type="text" id="type_test" name="type_test" size="3"></td>
									<td><input type="text" id="ruang" name="ruang" size="3"></td>
									<td><input type="text" id="lc" name="lc" size="1"></td>
									<td><input type="text" id="gc" name="gc" size="1"></td>
									<td><input type="text" id="rc" name="rc" size="1"></td>
									<td><input type="text" id="clc" name="clc" size="1"></td>
									<td><input type="text" id="cgc" name="cgc" size="1"></td>
									<td><input type="text" id="crc" name="crc" size="1"></td>
									<td><input type="text" id="total" name="total" size="1"></td>
									<td><input type="text" id="keterangan" name="keterangan" size="1"></td>
									
								   </tr> 
								   </table>
	                        </div>
	                    </div>
	                                 
	                </div>
	                <div class="modal-footer">
	                    <button class="btn btn-info" type="submit" value="ubah" name="ubah"> Simpan&nbsp;</button>
	                    <button type="button" class="btn btn-warning" data-dismiss="modal"> Batal</button>
	                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Ubah -->

    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
	
	 $(document).ready(function(){
         // Initiate DataTable function comes with plugin
         $('#dataTable').DataTable();
         // Start jQuery click function to view Bootstrap modal when view info button is clicked
            $('.view_data').click(function(){
             // Get the id of selected phone and assign it in a variable called phoneData
                var phoneData = $(this).attr('id');
                // Start AJAX function
                $.ajax({
                 // Path for controller function which fetches selected phone data
                    url: "<?php echo base_url() ?>testeprtittp/get_test_detail",
                    // Method of getting data
                    method: "POST",
                    // Data is sent to the server
                    data: {phoneData:phoneData},
                    // Callback function that is executed after data is successfully sent and recieved
                    success: function(data){
                     // Print the fetched data of the selected phone in the section called #phone_result 
                     // within the Bootstrap modal
                        $('#phone_result').html(data);
                        // Display the Bootstrap modal
                        $('#phoneModal').modal('show');
                    }
             });
             // End AJAX function
         });
     }); 
	 
	 $(document).ready(function() {
	        // Untuk sunting
	        $('#edit-data').on('show.bs.modal', function (event) {
	            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
	            var modal          = $(this)
 
	            // Isi nilai pada field
	            modal.find('#id_test').attr("value",div.data('id_test'));
				modal.find('#tanggal_test').attr("value",div.data('tanggal_test'));
				modal.find('#nim_nik').attr("value",div.data('nim_nik'));
				modal.find('#prodi').attr("value",div.data('prodi'));
				modal.find('#nama_lengkap').attr("value",div.data('nama_lengkap'));
				modal.find('#type_test').attr("value",div.data('type_test'));
				modal.find('#ruang').attr("value",div.data('ruang'));
				modal.find('#lc').attr("value",div.data('lc'));
				modal.find('#gc').attr("value",div.data('gc'));
				modal.find('#rc').attr("value",div.data('rc'));
				modal.find('#clc').attr("value",div.data('clc'));
				modal.find('#cgc').attr("value",div.data('cgc'));
				modal.find('#crc').attr("value",div.data('crc'));
				modal.find('#total').attr("value",div.data('total'));
				modal.find('#keterangan').attr("value",div.data('keterangan'));
				
							            
	        });
	    })
	 
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
	  
	  $(document).ready(function(){

 //load_data();

 function load_data()
 {
  $.ajax({
   url:"<?php echo base_url(); ?>excel_import/fetch",
   method:"POST",
   success:function(data){
    $('#customer_data').html(data);
   }
  })
 }

 $('#import_form').on('submit', function(event){
  event.preventDefault();
  $.ajax({
   url:"<?php echo base_url(); ?>testeprtittp/import",
   method:"POST",
   data:new FormData(this),
   contentType:false,
   cache:false,
   processData:false,
   success:function(data){
    $('#file').val('');
    //load_data();
    alert("Input Berhasil");
   }
  })
 });

});

  $(function() {
    $('#datetimepicker4').datepicker({
      pickTime: false,
	  format : "yyyy-mm-dd"
    });
  });
  
    $(function() {
    $('#datetimepicker5').datepicker({
      pickTime: false,
	  format : "yyyy-mm-dd"
    });
  });
  
  $(function() {
    $('#datetimepicker6').datepicker({
      pickTime: false,
	  format : "yyyy-mm-dd"
    });
  });
  
  $(function() {
    $('#datetimepicker7').datepicker({
      pickTime: false,
	  format : "yyyy-mm-dd"
    });
  });
  
$('#timepicker1').mdtimepicker({

  // format of the time value (data-time attribute)
  timeFormat: 'hh:mm', 

  // format of the input value
  format: 'hh:mm',      

  // theme of the timepicker
  // 'red', 'purple', 'indigo', 'teal', 'green'
  theme: 'blue',        

  // determines if input is readonly
  readOnly: false  ,

  // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
  hourPadding: true     

}); 

 $('#timepicker2').mdtimepicker({

  // format of the time value (data-time attribute)
  timeFormat: 'hh:mm', 

  // format of the input value
  format: 'hh:mm',      

  // theme of the timepicker
  // 'red', 'purple', 'indigo', 'teal', 'green'
  theme: 'blue',        

  // determines if input is readonly
  readOnly: false  ,

  // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
  hourPadding: true     

}); 
    </script>
</body>
</html>