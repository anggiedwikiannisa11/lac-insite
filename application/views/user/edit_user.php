<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>EDIT DATA USER</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM EDIT USER</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo base_url(); ?>user/updateuser" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">USERNAME</label>
                        <input type="hidden" class="form-control" value="<?php echo $id_user; ?>" id="" name="iduser" placeholder="Isika" required>
                        <input type="text" class="form-control" value="<?php echo $nama_user; ?>" id="" name="username" placeholder="" required>
                    </div>

                    <div class="form-group">
                      <label for="">NAMA</label>
                        <input type="text" class="form-control" value="<?php echo $nama; ?>" id="" name="nama" placeholder="" required>                        
                    </div>
                    <div class="form-group">
                      <label for="">PASSWORD</label>
                        <input type="password" class="form-control" value="" id="" name="password" placeholder="kosongkan jika tidak ingin di rubah" >                        
                    </div>                    
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group">
					<!--
						1 Admin Super
						2 Admin PResensi
						3 Administratif
						4 User Biasa
					-->
                      <label for="">LEVEL</label>
                        <select name="level" class="form-control" required>
                          <option>--Pilih Level--</option>
                              <option value="1" <?php if($level ==1){echo "selected";} ?> >1</option>
							  <option value="2" <?php if($level ==2){echo "selected";} ?> >2</option>
							  <option value="3" <?php if($level ==3){echo "selected";} ?> >3</option>
							  <option value="4" <?php if($level ==4){echo "selected";} ?> >4</option>
							  <option value="5" <?php if($level ==5){echo "selected";} ?> >5</option>
							  <option value="6" <?php if($level ==6){echo "selected";} ?> >6</option>
							  <option value="7" <?php if($level ==7){echo "selected";} ?> >7</option>
                        </select> 
                    </div>
                    
                    <div class="form-group">
					<!--
						1 Admin Super
						2 Admin PResensi
					-->
                      <label for="">STATUS</label>
                        <select name="status" class="form-control" required>
                          <option>--Pilih Status--</option>
                              <option value="1" <?php if($status ==1){echo "selected";} ?> >1</option>
							  <option value="2" <?php if($status ==2){echo "selected";} ?> >2</option>	
                        </select> 
                    </div>
                    
                  </div>
                  </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>user" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
               
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
        </div><!-- /.row (main row) -->

      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>Copyright &copy; 2018 <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
</body>
</html>