<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>Riwayat Cuti</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
			
        </section>

        <!-- Main content -->
        <section class="content">
		  
		   <?php if($this->session->userdata('level') ==1 || $this->session->userdata('level') ==3){ ?>
		  <div class="col-md-12">
				<div class="box">
				<h4><strong>Export</strong></h4>
					<form role="form" action="<?php echo base_url(); ?>laporan/cetak_cuti_csv" method="POST">
					<table  class="table table-bordered table-striped">
						<tr>
							
							<td>

									<input class="form-control" id="datetimepicker4" type="text" placeholder="Tanggal cuti 1" name ="tanggalTesEx1">
							
							</td>
							<td>

									-
							
							</td>
							<td>

									<input class="form-control" id="datetimepicker5" type="text" placeholder="Tanggal cuti 2" name ="tanggalTesEx2">
							
							</td>

							
							<td>
							<button type="submit" class="btn btn-warning">export</button>
							</td>
						</tr>
					</table>
				</form>
				</div>
		   </div>
		  <?php } ?>
		  
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
                <div class="box-title">
				
                </div><!-- /.box-title -->
                <div class="box-body">
				<form role="form" action="<?php echo base_url(); ?>cuti/viewcuti" method="POST">
					<table  class="table table-bordered table-striped">
						<tr>
							
							<td>
									<input class="form-control" id="datetimepicker6" type="text" placeholder="Tanggal cuti 1" name ="tanggalTesEx1"></input>

							</td>
							<td>
									-
							</td>
							<td>
									<input class="form-control" id="datetimepicker7" type="text" placeholder="Tanggal cuti 2" name ="tanggalTesEx2"></input>
							</td>

							<td>
							<button type="submit" class="btn btn-success">Lihat Data</button>
							</td>
						</tr>

						<tr >

						</tr>
					</table>
				</form>
				
				<br>
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
					<th>No</th>
                      <th>Nim</th>
                      <th>Nama</th>
                      <th>Keperluan</th>
					 
					  <th>Tanggal mulai cuti</th>
					  <th>Tanggal selesai cuti</th>
					   <th>Status</th>
					  <?php if( $this->session->userdata('level') !=4){ ?>
                      <th>Aksi</th>
					  <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_karyawan as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row['nippos']; ?></td>
                      <td><?php echo $row['nama_kar']; ?></td>
                      <td><?php echo $row['jenis_cuti']; ?></td>
					    <td><?php echo $row['tgl_cuti_from']; ?></td>
						  <td><?php echo $row['tgl_cuti_to']; ?></td>
					  <?php if($row['status']==0){  ?>
						<td><font color="yellow">Waiting</font></td>
					  <?php }else  if($row['status']==1){ ?>
						<td><font color="green">Approved</font></td>
					  <?php }else{ ?>
						<td><font color="red">Rejected</font></td>
					  <?php } ?>
					  <?php if( $this->session->userdata('level') !=4){ ?>
					  <td>
                     
					   <?php if($this->session->userdata('level') ==1){ ?>
					
					   <a 
								href="javascript:;"
								data-id_cuti="<?php echo  $row['id']; ?>"
								data-id_kar="<?php echo  $row['id_kar']; ?>"
								data-nippos="<?php echo  $row['nippos']; ?>"
								data-jabatan="<?php echo  $row['jabatan']; ?>"
								data-jumlah_cuti_karyawan="<?php echo  $row['jumlah_cuti_karyawan']; ?>"
								data-nama_kar="<?php echo  $row['nama_kar']; ?>"
								data-tgl_cuti_from="<?php echo  $row['tgl_cuti_from']; ?>"
								data-tgl_cuti_to="<?php echo  $row['tgl_cuti_to']; ?>"
								data-alamat_cuti="<?php echo  $row['alamat_cuti']; ?>"
								data-alasan_cuti="<?php echo  $row['alasan_cuti']; ?>"
								data-jml_hari_kerja="<?php echo  $row['jml_hari_kerja']; ?>"
								data-status="<?php echo  $row['status']; ?>"
								data-jenis_cuti="<?php echo  $row['jenis_cuti']; ?>"
								data-total_cuti="<?php echo  $row['jumlah_cuti_karyawan']-$totalCuti; ?>"
								data-tgl_approve="<?php echo  $row['tgl_approve']; ?>"
								data-toggle="modal" data-target="#edit-data">
                            <button  data-toggle="modal" data-target="#ubah-data" class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o"></i></button></a>
							</a>
					   <?php } ?>
                      </td>
					   <?php } ?>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>Copyright &copy; 2018 <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  
<!-- view Modal -->
 <div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="margin-top: -20px;">
   <div class="modal-dialog modal-lg">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 class="modal-title" id="myModalLabel">Test Details</h4>
       </div>
       <div class="modal-body">
        <!-- Place to print the fetched phone -->
         <div id="phone_result"></div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>
   </div>
 </div>
<!-- end of modal -->

<!-- Modal Ubah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Approve Data</h4>
            </div>
	            <div class="modal-body">
	                    <div class="form-group">
	                        <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
			
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM PENGAJUAN CUTI</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo base_url(); ?>cuti/updatecuti" method="POST" enctype="multipart/form-data">
                  <input type="hidden" id="id_cuti" name="id_cuti" />
				  <div class="col-lg-2">
                    <div class="form-group">
                      <label for="">NIP</label>
                
                    </div>

                    <div class="form-group">
                      <label for="">Nama</label>                                             
                    </div>
                    <div class="form-group">
                      <label for="">Jabatan</label>                                         
                    </div> 
					 <div class="form-group">
                      <label for="">Sisa Cuti</label>                                         
                    </div>
					 <div class="form-group">
                      <label for="">Jenis Keperluan</label>                                         
                    </div>
					<div class="form-group">
                      <label for="">Tanggal(dari-sampai)</label>                                         
                    </div>
					<div class="form-group">
                      <label for="">Jumlah hari kerja</label>                                         
                    </div>
					<div class="form-group">
                      <label for="">Alasan</label>                                         
                    </div>
					<div class="form-group">
                      <label for="">Alamat cuti</label>                                         
                    </div>
					<div class="form-group">
                      <label for="">Tanggal approve/reject</label>                                         
                    </div>
                  </div>
				  <!-- sebelah kanan -->
                  <div class="col-lg-10">
                    <div class="form-group">
                      <label for="" id="nippos"></label>                      
                    </div>
                     <div class="form-group">
                      <label id="nama_kar" for=""></label>                      
                    </div>
					<div class="form-group">
                      <label for="" id="jabatan"></label>                      
                    </div>
					<div class="form-group">
                      <label for="" id="total_cuti"></label>                      
                    </div>
					<div class="form-group">
                      <label for="" id="jenis_cuti"></label>                      
					</div>
					<div class="form-group">
                      <label for="" id="tgl_cuti_from"></label><label>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;</label><label for="" id="tgl_cuti_to">                   
                    </div>
					<div class="form-group">
                      <label for="" id="jml_hari_kerja">                  
                    </div>
					<div class="form-group">
                      <label for="" id="alasan_cuti">                  
                    </div>
					<div class="form-group">
                      <label for="" id="alamat_cuti">                  
                    </div>
					<div class="form-group">
                      <label for="" id="tgl_approve">                  
                    </div>
                  </div>
                  
                  
                </div><!-- /.item -->
            
	                 </div>
    </div>                        
		</div>
		<div class="modal-footer">
			<button class="btn btn-success" type="submit" value="approve" name="eksekusi"> Approve&nbsp;</button>
			 <button class="btn btn-danger" type="submit" value="reject" name="eksekusi"> Reject&nbsp;</button>
			 <button class="btn btn-info" type="submit" value="delete" name="eksekusi"> Delete&nbsp;</button>
			<button type="button" class="btn btn-warning" data-dismiss="modal"> Batal</button>
		</div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Ubah -->

    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
	
	 $(document).ready(function(){
         // Initiate DataTable function comes with plugin
         $('#dataTable').DataTable();
         // Start jQuery click function to view Bootstrap modal when view info button is clicked
            $('.view_data').click(function(){
             // Get the id of selected phone and assign it in a variable called phoneData
                var phoneData = $(this).attr('id');
                // Start AJAX function
                $.ajax({
                 // Path for controller function which fetches selected phone data
                    url: "<?php echo base_url() ?>testeprtsttpln/get_test_detail",
                    // Method of getting data
                    method: "POST",
                    // Data is sent to the server
                    data: {phoneData:phoneData},
                    // Callback function that is executed after data is successfully sent and recieved
                    success: function(data){
                     // Print the fetched data of the selected phone in the section called #phone_result 
                     // within the Bootstrap modal
                        $('#phone_result').html(data);
                        // Display the Bootstrap modal
                        $('#phoneModal').modal('show');
                    }
             });
             // End AJAX function
         });
     }); 
	 
	 $(document).ready(function() {
	        // Untuk sunting
	        $('#edit-data').on('show.bs.modal', function (event) {
	            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
	            var modal          = $(this)
// modal.find('.modal-body #kode').val(kode);
		// modal.find('.modal-body #time_label').text(time);
	            // Isi nilai pada field
				
	            modal.find('#id_cuti').attr("value",div.data('id_cuti'));
				modal.find('#id_kar').attr("value",div.data('id_kar'));
				
				modal.find('#nippos').text(div.data('nippos'));
				modal.find('#jabatan').text(div.data('jabatan'));
				modal.find('#jumlah_cuti_karyawan').attr("value",div.data('jumlah_cuti_karyawan'));
				modal.find('#nama_kar').text(div.data('nama_kar'));
				modal.find('#tgl_cuti_from').text(div.data('tgl_cuti_from'));
				modal.find('#tgl_cuti_to').text(div.data('tgl_cuti_to'));
				modal.find('#alamat_cuti').text(div.data('alamat_cuti'));
				modal.find('#alasan_cuti').text(div.data('alasan_cuti'));
				modal.find('#jml_hari_kerja').text(div.data('jml_hari_kerja'));
				modal.find('#status').text(div.data('status'));
				modal.find('#jenis_cuti').text(div.data('jenis_cuti'));
				//modal.find('#total_cuti').attr("text",div.data('total_cuti'));
				modal.find('#total_cuti').text(div.data('total_cuti'));
				modal.find('#tgl_approve').text(div.data('tgl_approve'));				
							            
	        });
	    })
	 
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
	  
	  $(document).ready(function(){

 //load_data();

 function load_data()
 {
  $.ajax({
   url:"<?php echo base_url(); ?>excel_import/fetch",
   method:"POST",
   success:function(data){
    $('#customer_data').html(data);
   }
  })
 }

 $('#import_form').on('submit', function(event){
  event.preventDefault();
  $.ajax({
   url:"<?php echo base_url(); ?>testeprtsttpln/import",
   method:"POST",
   data:new FormData(this),
   contentType:false,
   cache:false,
   processData:false,
   success:function(data){
    $('#file').val('');
    //load_data();
    alert("Input Berhasil");
   }
  })
 });

});

  $(function() {
    $('#datetimepicker4').datepicker({
      pickTime: false,
	  format : "yyyy-mm-dd"
    });
  });
  
    $(function() {
    $('#datetimepicker5').datepicker({
      pickTime: false,
	  format : "yyyy-mm-dd"
    });
  });
  
  $(function() {
    $('#datetimepicker6').datepicker({
      pickTime: false,
	  format : "yyyy-mm-dd"
    });
  });
  
  $(function() {
    $('#datetimepicker7').datepicker({
      pickTime: false,
	  format : "yyyy-mm-dd"
    });
  });
  
$('#timepicker1').mdtimepicker({

  // format of the time value (data-time attribute)
  timeFormat: 'hh:mm', 

  // format of the input value
  format: 'hh:mm',      

  // theme of the timepicker
  // 'red', 'purple', 'indigo', 'teal', 'green'
  theme: 'blue',        

  // determines if input is readonly
  readOnly: false  ,

  // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
  hourPadding: true     

}); 

 $('#timepicker2').mdtimepicker({

  // format of the time value (data-time attribute)
  timeFormat: 'hh:mm', 

  // format of the input value
  format: 'hh:mm',      

  // theme of the timepicker
  // 'red', 'purple', 'indigo', 'teal', 'green'
  theme: 'blue',        

  // determines if input is readonly
  readOnly: false  ,

  // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
  hourPadding: true     

}); 
    </script>
</body>
</html>