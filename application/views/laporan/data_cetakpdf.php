<?php
			$pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
			//$pdf->SetTitle('Daftar Produk');
			$pdf->SetHeaderMargin(30);
			$pdf->SetTopMargin(5);
			$pdf->setFooterMargin(20);
			$pdf->SetAutoPageBreak(true);
			$pdf->SetAuthor('Author');
			//$pdf->SetDisplayMode('real', 'default');
			$pdf->AddPage("L");
			$pdf->SetPrintFooter(false);
			$i=0;
			$namaPeg='';
			$html='
			<font size="8" face="Courier New" >
			<table cellspacing="1" cellpadding="1" bgcolor="#666666" >
				<tr bgcolor="#ffffff">
					<td rowspan="4" width="15%">INI LOGO</td>
					<td align="center" width="55%">PUSAT BAHASA TELKOM UNIVERSITY</td>
					<td width="15%">No. Dokumen</td>
					<td width="15%"></td>
				</tr>
				
				<tr bgcolor="#ffffff">
					<td align="center" width="55%">Jl. Telekomunikasi No.1 Ters. Buah Batu, Bandung 40257</td>
					<td width="15%">No. Revisi</td>
					<td width="15%"></td>
				</tr>
				
				<tr bgcolor="#ffffff">
					<td align="center" width="55%">Gedung Grha Wiyata Cacuk Sudarijanto-A, Lantai 1</td>
					<td width="15%">Berlaku efektif</td>
					<td width="15%"></td>
				</tr>
				
				<tr bgcolor="#ffffff">
					<td align="center" width="55%">Daftar Hadir Pegawai Pusat Bahasa Bulan '.$indMonthName.' '.$tahun.'</td>
					<td width="15%">Hal.</td>
					<td width="15%"></td>
				</tr>
				
			</table>
			</font>
			<font size="8" face="Courier New" >
			<table cellspacing="1" bgcolor="#666666" cellpadding="1">
				<tr bgcolor="#ffffff">
					<th width="5%" align="center">No</th>
					<th width="10%" align="center">ID Pegawai</th>
					<th width="30%" align="center">Nama</th>
					<th width="5%" align="center">Hari Kerja</th>
					<th width="10%" align="center">Tanggal Kerja</th>
					<th width="10%" align="center">Mulai Kerja</th>
					<th width="10%" align="center">Batas Waktu Masuk</th>
					<th width="10%" align="center">Selesai Kerja</th>
					<th width="10%" align="center">Jam Pulang Kantor</th>
				</tr>';
			foreach ($data_absensi as $row) 
				{
					$i++;
					$namaPeg=$row['nama_kar'];
					$html.='<tr bgcolor="'.$row['warna'].'">
							<td align="center">'.$i.'</td>
							<td>'.$row['karyawan_id'].'</td>
							<td>'.$row['nama_kar'].'</td>	
							<td align="center">'.date('D', strtotime($row['tanggal'])).'</td>
							<td align="center">'.$row['tanggal'].'</td>	
							<td align="center">'.$row['jam_masuk'].'</td>	
							<td align="center">'.$row['jam_kerja_masuk'].'</td>	
							<td align="center">'.$row['jam_keluar'].'</td>	
							<td align="center">'.$row['jam_kerja_keluar'].'</td>	

						</tr>';
				}
			$html.='</table>
					</font>';
			$html.='
			<font size="8" face="Courier New" >
			<table cellspacing="1" cellpadding="1" border=0; >
				<tr bgcolor="#ffffff">
					<td width="50%">&nbsp;&nbsp;&nbsp;&nbsp;Mengetahui,</td>
					<td width="50%">Bandung, '.date("d").' '.$indMonthNameNow.' '.$tahun.'</td>
				</tr>
				<tr bgcolor="#ffffff">
					<td width="50%">&nbsp;&nbsp;&nbsp;&nbsp;Pj Kabag Pusat Bahasa</td>
					<td width="50%">Pegawai,</td>
				</tr>
				<tr bgcolor="#ffffff">
					<td rowspan=2 width="50%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img align="center" vspace="20" style="width:80px;height:30px;left:70%;" src="'. base_url().'assets/upload/ttd_burita2.png" /></td>
					<td width="50%"></td>
				</tr>
				<tr bgcolor="#ffffff">
					<td width="50%"></td>
				</tr>
				<tr bgcolor="#ffffff">
					<td width="50%">&nbsp;&nbsp;&nbsp;&nbsp;Rita Destiwati, S.S., M.Si.</td>
					<td width="50%">'.$namaPeg.'</td>
				</tr>
			</table>
			</font>';
			// $html.='<p>Pegawai &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mengetahui</p>
					//<br>
					//<p>_________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________________';
			$pdf->writeHTML($html, true, false, true, false, '');
			$pdf->Output('PresensiTLH_'.$data_karyawan->nama_kar.'_'.$indMonthName.''.$tahun.'.pdf', 'I');
?>