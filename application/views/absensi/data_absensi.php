<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	    <form role="form" action="<?php echo base_url(); ?>absensi/viewPresensiKaryawan" method="POST">
		<input type="hidden" value="<?php echo $id_kar ?>" name="id_kar">
        <h1>
          <b>DATA PRESENSI <?php echo $data_karyawan->nama_kar ?></b>
          <select class="form-control" style="width:300px" name="bulan">
          	<option value="">--Pilih Bulan--</option>
          	<option value="01" <?php if($bulan == '01'){echo 'selected';}  ?> >Januari</option>
			<option value="02" <?php if($bulan == '02'){echo 'selected';}  ?> >Februari</option>
          	<option value="03" <?php if($bulan == '03'){echo 'selected';}  ?> >Maret</option>
			<option value="04" <?php if($bulan == '04'){echo 'selected';}  ?> >April</option>
			<option value="05" <?php if($bulan == '05'){echo 'selected';}  ?> >Mei</option>
			<option value="06" <?php if($bulan == '06'){echo 'selected';}  ?> >Juni</option>
			<option value="07" <?php if($bulan == '07'){echo 'selected';}  ?> >Juli</option>
			<option value="08" <?php if($bulan == '08'){echo 'selected';}  ?> >Agustus</option>
			<option value="09" <?php if($bulan == '09'){echo 'selected';}  ?> >September</option>
			<option value="10" <?php if($bulan == '10'){echo 'selected';}  ?> >Oktober</option>
			<option value="11" <?php if($bulan == '11'){echo 'selected';}  ?> >November</option>
			<option value="12" <?php if($bulan == '12'){echo 'selected';}  ?> >Desember</option>
          </select>
          <select class="form-control" style="width:300px" name="tahun">
          	<option value="">--Pilih Tahun--</option>
          	<option value="2018" <?php if($tahun == '2018'){echo 'selected';}  ?> >2018</option>
          	<option value="2019" <?php if($tahun == '2019'){echo 'selected';}  ?> >2019</option>
          </select><br>
          <button type="submit" class="btn btn-success">Lihat Data</button>
		 <!-- <button type="submit" class="btn btn-warning"><i class="fa fa-print"> Cetak</i></button> -->
		  <a class="btn btn-warning" target="_blank" href="<?php echo base_url(); ?>laporan/cetak_presensi_pdf/<?php echo $id_kar ?>/<?php echo $bulan ?>/<?php echo $tahun; ?> ">Download PDF</a>
		   <a class="btn btn-warning" target="_blank" href="<?php echo base_url(); ?>laporan/cetak_presensi_csv/<?php echo $id_kar ?>/<?php echo $bulan ?>/<?php echo $tahun; ?> ">Download CSV</a>

        </h1>
		</form>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
            <!-- <a style="margin-bottom:3px" href="<?php echo base_url(); ?>karyawan/addkaryawan" class="btn btn-primary no-radius dropdown-toggle"><i class="fa fa-plus"></i> TAMBAH KARYAWAN </a> -->
              <div class="box">
                <!-- <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span> -->
                <div class="box-title">
                  
                </div><!-- /.box-title -->
                <div class="box-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>NIP</th>
                      <th>NAMA</th>
                      <th>TANGGAL</th>
					  <th>Jam Masuk Kerja</th>
					  <th>Jam Masuk</th>
					  <th>Jam Selesai Kerja</th>
					  <th>Jam Keluar</th>
					  <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($data_absensi as $row) { $no++ ?>
                    <tr >
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row['karyawan_id']; ?></td>
                      <td><?php echo $row['nama_kar']; ?></td>
                      <td><?php echo $row['tanggal']; ?>  <?php echo date('D', strtotime($row['tanggal'])); ?></td>
                      <td><?php echo $row['jam_kerja_masuk']; ?></td>
					  <td><?php echo $row['jam_masuk']; ?></td>
					  <td><?php echo $row['jam_kerja_keluar']; ?></td>
					  <td><?php echo $row['jam_keluar']; ?></td>
					  <td><a 
                            href="javascript:;"
                            data-karyawanid="<?php echo $row['karyawan_id'] ?>"
                            data-jammasuk="<?php echo $row['jam_masuk'] ?>"
                            data-jamkeluar="<?php echo $row['jam_keluar'] ?>"
							data-nama="<?php echo $row['nama_kar'] ?>"
							data-tanggal="<?php echo $row['tanggal'] ?>"
							data-idkar="<?php echo $row['id_kar'] ?>"
							data-presensiid="<?php echo $row['presensi_id'] ?>"
							data-bulan="<?php echo $bulan ?>"
							data-tahun="<?php echo $tahun ?>"
                            data-toggle="modal" data-target="#edit-data">
                            <button  data-toggle="modal" data-target="#ubah-data" class="btn btn-info">Ubah</button>
                        </a></td>
                      <!-- <td><?php if ($row['kodeabsensi'] == 1) { ?>
						<h4><span class="label label-success">Masuk</span></h4>
                      <?php } else { ?>
                      <h4><span style="text-fonts:16px" class="label label-danger">Pulang</span></h4>
                      <?php } ?>
                      </td> -->
                      
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
	
	<!-- Modal Ubah -->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
	                <h4 class="modal-title">Ubah Data Presensi</h4>
	            </div>
	            <form class="form-horizontal" action="<?php echo base_url('absensi/editPresensiKaryawan')?>" method="post" enctype="multipart/form-data" role="form">
		            <div class="modal-body">
		                    <div class="form-group">
		                        <label class="col-lg-2 col-sm-2 control-label">Nama :</label>
		                        <div class="col-lg-10">
		                        	<input type="hidden" id="karyawan_id" name="karyawan_id">
									<input type="hidden" id="tanggal" name="tanggal">
									<input type="hidden" id="id_kar" name="id_kar">
									<input type="hidden" id="presensi_id" name="presensi_id">
									<input type="hidden" id="bulan" name="bulan">
									<input type="hidden" id="tahun" name="tahun">
									<label class="col-lg-4 col-sm-4 control-label" id="nama"></label>
		                            <!-- <input type="text" class="form-control" id="nama" name="nama" placeholder="Tuliskan Nama"> -->
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-lg-2 col-sm-2 control-label">tanggal :</label>
		                        <div class="col-lg-10">
		                        	<label class="col-lg-4 col-sm-4 control-label" id="tanggallab"></label>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-lg-2 col-sm-2 control-label">Jam Masuk</label>
		                        <div class="col-lg-10">
		                            <input type="text" class="form-control" id="jam_masuk" name="jam_masuk" >
		                        </div>
		                    </div>
							 <div class="form-group">
		                        <label class="col-lg-2 col-sm-2 control-label">Jam Keluar</label>
		                        <div class="col-lg-10">
		                            <input type="text" class="form-control" id="jam_keluar" name="jam_keluar" >
		                        </div>
		                    </div>
		                </div>
		                <div class="modal-footer">
		                    <button class="btn btn-info" type="submit"> Simpan&nbsp;</button>
		                    <button type="button" class="btn btn-warning" data-dismiss="modal"> Batal</button>
		                </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- END Modal Ubah -->
	
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>Copyright &copy; 2018 <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
	  $(document).ready(function() {
		// Untuk sunting
		$('#edit-data').on('show.bs.modal', function (event) {
			var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
			var modal          = $(this)

			// Isi nilai pada field
			modal.find('#karyawan_id').attr("value",div.data('karyawanid'));
			modal.find('#id_kar').attr("value",div.data('idkar'));
			modal.find('#presensi_id').attr("value",div.data('presensiid'));
			modal.find('#bulan').attr("value",div.data('bulan'));
			modal.find('#tahun').attr("value",div.data('tahun'));
			modal.find('#nama').text(div.data('nama'));
			modal.find('#tanggallab').text(div.data('tanggal'));
			modal.find('#tanggal').attr("value",div.data('tanggal'));
			modal.find('#jam_masuk').attr("value",div.data('jammasuk'));
			modal.find('#jam_keluar').attr("value",div.data('jamkeluar'));
		});
	});
		
      $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false


        });
      });
            //waktu flash data :v
      $(function(){
      $('#pesan-flash').delay(4000).fadeOut();
      $('#pesan-error-flash').delay(5000).fadeOut();
      });
    </script>
</body>
</html>