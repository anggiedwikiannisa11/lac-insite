<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url(); ?>assets/dist/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <?php $this->load->view('inc/head'); ?>
  
</head>
<body class="skin-blue">
  <!-- wrapper di bawah footer -->
  <div class="wrapper">

    <?php $this->load->view('inc/head2'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <?php $this->load->view('inc/sidebar'); ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <b>PENGAJUAN CUTI</b>
        </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
		   <span id="pesan-flash"><?php echo $this->session->flashdata('sukses'); ?></span>
                <span id="pesan-error-flash"><?php echo $this->session->flashdata('alert'); ?></span>
          <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">FORM PENGAJUAN CUTI</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="item">
                  <form role="form" action="<?php echo base_url(); ?>karyawan/savedata" method="POST" enctype="multipart/form-data">
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label for="">NIP</label>
                       
                    </div>

                    <div class="form-group">
                      <label for="">Nama</label>                                             
                    </div>
                    <div class="form-group">
                      <label for="">Jabatan</label>                                         
                    </div> 
					  <div class="form-group">
                      <label for="">Cuti Terpakai</label>                                         
                    </div> 
                  </div>
				  <!-- sebelah kanan -->
                  <div class="col-lg-10">
                    <div class="form-group">
                      <label for=""><?php echo $data_karyawan[0]['nippos']; ?></label>                      
                    </div>
                     <div class="form-group">
                      <label for=""><?php echo $nama; ?></label>                      
                    </div>
					<div class="form-group">
                      <label for=""><?php echo $data_karyawan[0]['jabatan']; ?></label>                      
                    </div>
						<div class="form-group">
                      <label for=""><?php $cuti= $data_karyawan[0]['jml_cuti']; echo $cuti-$totalCuti; ?></label>                      
                    </div>
                  </div>
                  
                  
                </div><!-- /.item -->
                <div class="form-group">
                  <!-- <button type="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
                  <a href="<?php echo base_url(); ?>karyawan" class="btn btn-warning btn-block btn-flat">Kembali</a> -->
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
		  <!-- Bagian bawah -->
		  
		  <section class="col-lg-12">
            <!-- Chat box -->
            <div class="box">
              <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">DETAIL PENGAJUAN CUTI</h3>
              </div>
              <div class="box-body chat" id="chat-box">
                <!-- chat item -->
				 <form role="form" action="<?php echo base_url(); ?>cuti/savedata" method="POST" enctype="multipart/form-data">
				 <div class="form-group">
                      <label for="">Jenis</label>
                        <select name="jenis" class="form-control" required>
                          <option>--Pilih Keperluan--</option>
                              <option value="IZIN">Izin</option>
							  <option value="CUTI">Cuti</option>
							  <option value="SAKIT">Sakit</option>
                        </select> 
                    </div>
                <div class="item">
                
				 <input type="hidden" name="id_kar" value="<?php echo $data_karyawan[0]['id_kar']; ?>" >
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Mulai</label>
                        <input type="text" class="form-control" value="" id="datetimepicker1" name="tgl_mulai" placeholder="" required>
                    </div>

                    <div class="form-group">
                      <label for="">Jumlah hari kerja</label>
                        <input type="text" class="form-control" value="" id="" name="jml_kerja" placeholder="" required>                        
                    </div>
                    <div class="form-group">
                      <label for="">Alamat Selama Cuti</label>
                        <textarea class="form-control" value="" id="" name="alamat_cuti" placeholder="" required>  </textarea>                      
                    </div>                    
                  </div>
				  
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="">Sampai dengan</label>
                        <input type="text" class="form-control" value="" id="datetimepicker2" name="tgl_selesai" placeholder="" required>
                    </div>
					 <div class="form-group">
                      <label for="">Alasan</label>
                        <textarea class="form-control" value="" id="" name="alasan" placeholder="" required>  </textarea>                      
                    </div>  

                    
                  </div>

                </div><!-- /.item -->
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                  <a href="<?php echo base_url(); ?>karyawan" class="btn btn-warning btn-block btn-flat">Kembali</a>
                </div><!-- /.col -->
               </form>
              </div><!-- /.chat -->
            </div><!-- /.box (chat box) -->
          </section><!-- /.Left col -->
		  
          <section class="col-lg-5 connectedSortable">

          </section><!-- right col -->
		  
		  
		  
        </div><!-- /.row (main row) -->
		
		
		
		
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.0 -->
      </div>
      <strong>Copyright &copy; 2018 <a href="#"></a></strong>
    </footer>
  </div><!-- ./wrapper -->
  <!-- page script -->
  

    
    <?php $this->load->view('inc/footer'); ?>
	<script type="text/javascript">

  $('#datetimepicker1').datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        linkField: "mirror_field",
        linkFormat: "yyyy-mm-dd hh:ii:ss"
    });
  
    $('#datetimepicker2').datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        linkField: "mirror_field",
        linkFormat: "yyyy-mm-dd hh:ii:ss"
    });
</script>
</body>
</html>