<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	//ambil data user
	function GetUser($data) {
        $this->db->where('nama_user', $data['nama_user']);
		$this->db->like('pass_user', $data['pass_user'], 'both'); 
		$query = $this->db->get('tb_login');
        return $query;
    }

	//ambil data tabel jabatan
	public function GetJab($where= "")
	{
		$data = $this->db->query('select * from tb_jabatan '.$where);
		return $data;
	}
	//ambil data tabel produk
	public function GetKaryawan($where= "")
	{
		$data = $this->db->query('select * from tb_karyawan '.$where);
		return $data;
	}

	public function GetTotKaryawan()
	{
		$data = $this->db->query('select * from tb_karyawan group by id_jab ');
		return $data;
	}

	public function GetDetAbsensi($where = ""){
		return $this->db->query("select tb_absensi.*, tb_karyawan.*, tb_jabatan.* from tb_karyawan inner join tb_absensi on tb_absensi.nippos=tb_karyawan.nippos $where;");
	}

	public function count_all() {
		return $this->db->count_all('tb_karyawan');
	}

	//ambil data dari 3 tabel
	public function GetKaryawanJabAbs($where= "") {
		$sql = 'SELECT p.*, q.jabatan, r.*
                                FROM tb_karyawan p
                                INNER JOIN tb_presensi r
                                ON(p.nippos = r.karyawan_id)
                                INNER JOIN tb_jabatan q
                                ON(p.id_jab = q.id_jab)'.$where ;
			//var_dump($sql);die();
    $data = $this->db->query('SELECT p.*, q.jabatan, r.*
                                FROM tb_karyawan p
                                INNER JOIN tb_presensi r
                                ON(p.nippos = r.karyawan_id)
                                INNER JOIN tb_jabatan q
                                ON(p.id_jab = q.id_jab)'.$where);
    return $data;
    }

	//ambil data dari 2 tabel
	public function GetKaryawanJab($where= "") {
    $data = $this->db->query('SELECT p.*, q.jabatan, r.id_kar as cek_aktif
                                FROM tb_karyawan p
                                LEFT JOIN tb_jabatan q
                                ON(p.id_jab = q.id_jab)
								LEFT JOIN tb_login r ON(p.id_kar = r.id_kar)
								'.$where);
    return $data;
    }

	//batas crud data
	public function Simpan($table, $data){
		return $this->db->insert($table, $data);
	}

	public function Update($table,$data,$where){
		return $this->db->update($table,$data,$where);
	}

	public function Hapus($table,$where){
		return $this->db->delete($table,$where);
	}

	function UpdateKaryawan($data){
		//var_dump($data);die();
        $this->db->where('id_kar',$data['id_kar']);
        $this->db->update('tb_karyawan',$data);
    }
	//batas crud data

 //    //model untuk visitor/pengunjung
 //    function GetVisitor($where = ""){
	// 	return $this->db->query("select * from tb_visitor $where");		
	// }

	function GetProductView(){
		return $this->db->query("select sum(counter) as totalview from tb_karyawan where status = 'publish'");
	}
	//batas query pengunjung

	public function GetJabe($where= "")
	{
		$data = $this->db->query('select count(*) as totaljabatan from tb_jabatan '.$where);
		return $data;
	}

	function TotalKat(){
		return $this->db->query("select count(*) as totaljabatan from tb_karyawan group by id_jab; ");
	}
	
	public function GetIndonesianDateNeame($id)
	{
		$dateName = "";
		if($id=="01"){$dateName = "Januari";};
		if($id=="02"){$dateName = "Februari";};
		if($id=="03"){$dateName = "Maret";};
		if($id=="04"){$dateName = "April";};
		if($id=="05"){$dateName = "Mei";};
		if($id=="06"){$dateName = "Juni";};
		if($id=="07"){$dateName = "Juli";};
		if($id=="08"){$dateName = "Agustus";};
		if($id=="09"){$dateName = "September";};
		if($id=="10"){$dateName = "Oktober";};
		if($id=="11"){$dateName = "November";};
		if($id=="12"){$dateName = "Desember";};
		
		return $dateName;
	}
	public function GetIndonesianDateNameToNumber($id)
	{
		$dateName = "";
		if($id=="Januari"){$dateName = "01";};
		if($id=="Februari"){$dateName = "02";};
		if($id=="Maret"){$dateName = "03";};
		if($id=="April"){$dateName = "04";};
		if($id=="Mei"){$dateName = "05";};
		if($id=="Juni"){$dateName = "06";};
		if($id=="Juli"){$dateName = "07";};
		if($id=="Agustus"){$dateName = "08";};
		if($id=="September"){$dateName = "09";};
		if($id=="Oktober"){$dateName = "10";};
		if($id=="November"){$dateName = "11";};
		if($id=="Desember"){$dateName = "12";};
		
		return $dateName;
	}
	
	function UpdatePresensi($data){
		//var_dump($data);die();
        $this->db->where('karyawan_id',$data['karyawan_id']);
		$this->db->where('tanggal',$data['tanggal']);
        $this->db->update('tb_presensi',$data);
		return $data;
    }
	
	public function GetJamKerjaDefault ($where= "")
	{
		$data = $this->db->query('select * from tb_jam_kerja_default '.$where);
		return $data;
	}
	
	public function GetDataTest($where="")
	{
		$sql = "select * from tb_test".$where;
		//var_dump($where);die();
		// $data = $this->db->query('select a.*,ifnull(b.cetakan,0) cetakan,c.no_sertifikat_comp from tb_test a 
								// Left JOIN (select count(*) as cetakan, id_test from tb_cetak_sertifikat group by id_test) b 
								// on (a.id_test=b.id_test) 
								// Left JOIN (select no_sertifikat_comp, id_test from tb_cetak_sertifikat   order by id_sertifikat desc LIMIT 1 ) c
								// on (a.id_test=c.id_test) where a.deleted_date is null '.$where);
		$data = $this->db->query('select * from tb_test a 
								 where a.deleted_date is null '.$where);
								
		return $data;
	}
	
	public function GetDataTestEcct($where="")
	{

		$data = $this->db->query('select * from tb_test_ecct a 
								 where a.deleted_date is null '.$where);
								
		return $data;
	}
	
	public function GetDataTestEcctSmkTel($where="")
	{

		$data = $this->db->query('select * from tb_test_ecct_smk_telkom a 
								 where a.deleted_date is null '.$where);
								
		return $data;
	}
	
	public function GetSertifiatInc()
	{		
		$sql = "select id_sertifikat,max(sertifikat_no) as no_sertifikat,id_test from tb_cetak_sertifikat";
		$result = $this->db->query($sql)->first_row();
		
		return $result;
	}
	
	public function Insert($table,$data)
	{
		 $this->db->insert_batch($table, $data);
		 return $data;
	}
	
	public function GetDataUser($where= "")
	{
		$data = $this->db->query('select * from tb_login '.$where);
		return $data;
	}
	
	function UpdateUser($data){
		//var_dump($data);die();
        $this->db->where('id_user',$data['id_user']);
        $this->db->update('tb_login',$data);
    }
	
	function UpdateTest($data){
		//var_dump($data);die();
        $this->db->where('id_test',$data['id_test']);
        return $this->db->update('tb_test',$data);
    }
	
	function UpdateTestEcct($data){
		//var_dump($data);die();
        $this->db->where('id_test_ecct',$data['id_test_ecct']);
        return $this->db->update('tb_test_ecct',$data);
    }
	
	function UpdateTestEcctSmkTel($data){
		//var_dump($data);die();
        $this->db->where('id_test_ecct',$data['id_test_ecct']);
        return $this->db->update('tb_test_ecct_smk_telkom',$data);
    }
	
	public function GetLastNoCertificate()
	{
		$tahun = date('Y');
		$sql = "select max(no_sertifikat) as noser from tb_test where year(tanggal_test)='$tahun'";
		$result = $this->db->query($sql)->first_row();
		
		return $result;
	}
	
	public function GetLastNoCertificateEcct()
	{	
		$tahun = date('Y');
		$sql = "select max(no_sertifikat) as noser from tb_test_ecct where year(tanggal_test)='$tahun'";
		$result = $this->db->query($sql)->first_row();
		
		return $result;
	}
	
	public function GetLastNoCertificateEcctSmkTel()
	{	
		$tahun = date('Y');
		$sql = "select max(no_sertifikat) as noser from tb_test_ecct_smk_telkom where year(tanggal_test)='$tahun'";
		$result = $this->db->query($sql)->first_row();
		
		return $result;
	}
	
	public function GetDataTestEprtIttp($where="")
	{
		$sql = "select * from tb_test_eprt_ittp".$where;

		$data = $this->db->query('select * from tb_test_eprt_ittp a 
								 where a.deleted_date is null '.$where);
								
		return $data;
	}
	
	function UpdateTestEprtIttp($data){
		//var_dump($data);die();
        $this->db->where('id_test',$data['id_test']);
        return $this->db->update('tb_test_eprt_ittp',$data);
    }
	
	public function GetLastNoCertificateEprtIttp()
	{
		$tahun = date('Y');
		$sql = "select max(no_sertifikat) as noser from tb_test_eprt_ittp where year(tanggal_test)='$tahun'";
		$result = $this->db->query($sql)->first_row();
		
		return $result;
	}
	
	
	public function GetDataTestEprtSttpln($where="")
	{
		$sql = "select * from tb_test_eprt_sttpln".$where;

		$data = $this->db->query('select * from tb_test_eprt_sttpln a 
								 where a.deleted_date is null '.$where);
								
		return $data;
	}
	
	function UpdateTestEprtSttpln($data){
		//var_dump($data);die();
        $this->db->where('id_test',$data['id_test']);
        return $this->db->update('tb_test_eprt_sttpln',$data);
    }
	
	public function GetLastNoCertificateEprtSttpln()
	{		
		$tahun = date('Y');
		$sql = "select max(no_sertifikat) as noser from tb_test_eprt_sttpln where year(tanggal_test)='$tahun'";
		$result = $this->db->query($sql)->first_row();
		
		return $result;
	}
	
	public function GetDataTestEprtSmaTelBdg($where="")
	{
		$sql = "select * from tb_test_eprt_sma_telkom_bdg".$where;

		$data = $this->db->query('select * from tb_test_eprt_sma_telkom_bdg a 
								 where a.deleted_date is null '.$where);
								
		return $data;
	}
	
	function UpdateTestEprtSmaTelBdg($data){
		//var_dump($data);die();
        $this->db->where('id_test',$data['id_test']);
        return $this->db->update('tb_test_eprt_sma_telkom_bdg',$data);
    }
	
	public function GetLastNoCertificateEprtSmaTelBdg()
	{		
		$tahun = date('Y');
		$sql = "select max(no_sertifikat) as noser from tb_test_eprt_sma_telkom_bdg where year(tanggal_test)='$tahun'";
		$result = $this->db->query($sql)->first_row();
		
		return $result;
	}
	
	public function GetCountCutiTahunan($where="")
	{		
		$sql = "select sum(jml_cuti) as jml_cuti from tb_cuti $where";
		$result = $this->db->query($sql)->first_row();
		
		return $result;
	}
	
	public function GetCuti($where = ""){
		$sql = "SELECT a.*,b.nippos,b.nama_kar,b.jml_cuti as jumlah_cuti_karyawan,c.jabatan from tb_cuti a
				LEFT JOIN tb_karyawan b ON(a.id_kar = b.id_kar)
				LEFT JOIN tb_jabatan c ON(b.id_jab = c.id_jab) $where order by a.id desc";
		
		$data = $this->db->query($sql);
		return $data;
	}
	
	function UpdateCuti($data){
		//var_dump($data);die();
        $this->db->where('id',$data['id']);
        $this->db->update('tb_cuti',$data);
    }
}

/* End of file model.php */
/* Location: ./application/models/model.php */