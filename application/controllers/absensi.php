<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absensi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
	}
	private function _cek_login()
	{
		if(!$this->session->userdata('useradmin')){            
			redirect(base_url().'backend');
		}
	}

	public function index()
	{
		$data = array(
			'data_absensi' => $this->model->GetKaryawanJabAbs("order by presensi_id desc")->result_array(),
			'nama' => $this->session->userdata('nama'),	
		);
		
		$this->load->view('absensi/data_absensi', $data);
	}
	
	public function viewPresensiKaryawan()
	{

		$id_kar = $this->uri->segment(3);
		
		$bulan_=$this->input->post('bulan');
		$tahun_=$this->input->post('tahun');
		$id_kar_=$this->input->post('id_kar');
		
		if($bulan_){
			$idKar = isset($id_kar_)? $id_kar_ : $id_kar = $this->uri->segment(3);;
		}
		else
		{
			$idKar = $this->uri->segment(3);
		}
		
		if($bulan_){
			$bulan = isset($bulan_)? $bulan_ : date('m');
		}
		else
		{
			if($bulanCheck = $this->uri->segment(4))
			{
				$bulan = $this->uri->segment(4);
			}
			else{
			$bulan =  date('m');
			}
		}
		
		if($tahun_){
			$tahun = isset($tahun_)? $tahun_ : date('Y');
		}
		else
		{
			if($tahunCheck = $this->uri->segment(5))
			{
				$tahun = $this->uri->segment(5);
			}
			else{
			$tahun =  date('Y');
			}
		}
		$data_karyawan = $this->model->GetKaryawan("where id_kar=".$idKar."")->first_row();
		$data_absensi = $this->model->GetKaryawanJabAbs("where id_kar=".$idKar." and month(periode)='".$bulan."' and year(periode)='".$tahun."' order by presensi_id desc")->result_array();
		// Start date
		$date = $tahun."-".$bulan."-01";
		$endDate=strtotime($date) ;
		
		$dataTemp = array();
		$jamKerjaDefault = $this->model->GetJamKerjaDefault("")->first_row();
		while (strtotime($date) < strtotime("+1 month",$endDate)) {
                //echo "$date\n";
				if(empty($data_absensi)){
					$dataTemp[$date]['karyawan_id'] =$data_karyawan->nippos; 
					$dataTemp[$date]['nama_kar'] =$data_karyawan->nama_kar; 
					$dataTemp[$date]['tanggal'] =$date; 
					$dataTemp[$date]['jam_kerja_masuk'] = $jamKerjaDefault->jam_kerja_masuk; 
					$dataTemp[$date]['jam_masuk'] ="";
					$dataTemp[$date]['jam_kerja_keluar'] =$jamKerjaDefault->jam_kerja_keluar; 
					$dataTemp[$date]['jam_keluar'] = ""; 
					$dataTemp[$date]['presensi_id'] = ""; 
					$dataTemp[$date]['id_kar'] =$data_karyawan->id_kar; 
					
					$hari = date('D', strtotime($date));

					if($hari=="Sun" || $hari=="Sat" )
					{
						$dataTemp[$date]['warna'] = "gray";
					}
					else
					{
						$dataTemp[$date]['warna'] = "white";
					}
				}
				else{
					foreach($data_absensi as $row){
						if($date==$row['tanggal'])
						{
							$dataTemp[$date]['karyawan_id'] =$row['karyawan_id']; 
							$dataTemp[$date]['nama_kar'] =$row['nama_kar']; 
							$dataTemp[$date]['tanggal'] =$row['tanggal']; 
							$dataTemp[$date]['jam_kerja_masuk'] = date('H:i:s', strtotime($row['jam_kerja_masuk']));
							$dataTemp[$date]['jam_masuk'] =$row['jam_masuk']==null?"":date('H:i:s', strtotime($row['jam_masuk']));
							$dataTemp[$date]['jam_kerja_keluar'] =date('H:i:s', strtotime($row['jam_kerja_keluar']));
							$dataTemp[$date]['jam_keluar'] = $row['jam_keluar']==null ?"":date('H:i:s', strtotime($row['jam_keluar']));
							$hari = date('D', strtotime($date));
							$dataTemp[$date]['presensi_id'] = $row['presensi_id']; 
							$dataTemp[$date]['id_kar'] = $row['id_kar']; 
							
							if($hari=="Sun" || $hari=="Sat" )
							{
								$dataTemp[$date]['warna'] = "gray";
							}
							else
							{
								$dataTemp[$date]['warna'] = "white";
							}
							break;
						}
						else
						{
							//var_dump($date);die();
							$dataTemp[$date]['karyawan_id'] =$data_karyawan->nippos; 
							$dataTemp[$date]['nama_kar'] =$data_karyawan->nama_kar; 
							$dataTemp[$date]['tanggal'] =$date; 
							$dataTemp[$date]['jam_kerja_masuk'] = $jamKerjaDefault->jam_kerja_masuk; 
							$dataTemp[$date]['jam_masuk'] ="";
							$dataTemp[$date]['jam_kerja_keluar'] =$jamKerjaDefault->jam_kerja_keluar; 
							$dataTemp[$date]['jam_keluar'] = ""; 
							$dataTemp[$date]['presensi_id'] = ""; 
							$dataTemp[$date]['id_kar'] =$data_karyawan->id_kar;
							
							$hari = date('D', strtotime($date));
	
							if($hari=="Sun" || $hari=="Sat" )
							{
								$dataTemp[$date]['warna'] = "gray";
							}
							else
							{
								$dataTemp[$date]['warna'] = "white";
							}
						}
					}
				}
                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
		}
		
		//var_dump($dataTemp );die();
		$data = array(
			'data_absensi' => $dataTemp,
			'data_karyawan' => $data_karyawan,	
			'nama' => $this->session->userdata('nama'),	
			'id_kar' => $idKar,
			'bulan' => $bulan,	
			'tahun' => $tahun,			
		);
		
		$this->load->view('absensi/data_absensi', $data);
	}
	
	public function editPresensiKaryawan()
	{
		$idKar = $this->input->post('id_kar');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$presensiId = $this->input->post('presensi_id');
		//var_dump($presensiId);die();
		//var_dump($idKar );die();
		if(!isset($presensiId) || trim($presensiId) === ''){
			$jamKerjaDefault = $this->model->GetJamKerjaDefault("")->first_row();
			$d = new DateTime($this->input->post('tanggal'));
			$periode = $d->format("Y-m-1");
			//var_dump($jamKerjaDefault );die();
			$data = array(
				'karyawan_id'	=> $this->input->post('karyawan_id'),
				'jam_kerja_masuk'	=> $this->input->post('tanggal'). " ".$jamKerjaDefault->jam_kerja_masuk,
				'jam_masuk'		=> $this->input->post('tanggal'). " ".$this->input->post('jam_masuk'),
				'jam_kerja_keluar'	=> $this->input->post('tanggal'). " ".$jamKerjaDefault->jam_kerja_keluar,
				'jam_keluar'	=> $this->input->post('tanggal'). " ".$this->input->post('jam_keluar'),
				'tanggal'	=> $this->input->post('tanggal'),
				'periode'	=> $periode
			);
			$this->model->Simpan("tb_presensi",$data);
		}
		else{
			$data = array(
				'karyawan_id'	=> $this->input->post('karyawan_id'),
				'jam_masuk'		=> $this->input->post('tanggal'). " ".$this->input->post('jam_masuk'),
				'jam_keluar'	=> $this->input->post('tanggal'). " ".$this->input->post('jam_keluar'),
				'tanggal'	=> $this->input->post('tanggal')
			);
			//var_dump($data);die();
			$this->model->UpdatePresensi($data);
			$this->session->set_flashdata('notif','<div class="alert alert-success" role="alert"> Data Berhasil diubah <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		}
		redirect('absensi/viewPresensiKaryawan/'.$idKar.'/'.$bulan.'/'.$tahun);
	}
}
