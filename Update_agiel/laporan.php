<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public function __construct()
		{	
			parent::__construct();
			$this->load->library('Pdf');
			$this->load->library('Ciqrcode');
			//$this->load->library('encryption');
		}
	public function contoh()
		{
			$this->load->view('laporan/cetakpdf', $data);
		}
	public function cetak_presensi_pdf()
		{
		$idKar = $this->uri->segment(3);
		$bulan=$this->uri->segment(4);
		$tahun=$this->uri->segment(5);
		
		// $bulan_=$this->input->post('bulan');
		// $tahun_=$this->input->post('tahun');
		// $id_kar_=$this->input->post('id_kar');
		
		// if($bulan_){
			// $idKar = isset($id_kar_)? $id_kar_ : $id_kar = $this->uri->segment(3);;
		// }
		// else
		// {
			// $idKar = $this->uri->segment(3);
		// }
		
		// if($bulan_){
			// $bulan = isset($bulan_)? $bulan_ : date('m');
		// }
		// else
		// {
			// $bulan =  date('m');
		// }
		
		// if($tahun_){
			// $tahun = isset($tahun_)? $tahun_ : date('Y');
		// }
		// else
		// {
			// $tahun =  date('Y');
		// }
		$data_karyawan = $this->model->GetKaryawan("where id_kar=".$idKar."")->first_row();
		$data_absensi = $this->model->GetKaryawanJabAbs("where id_kar=".$idKar." and month(periode)='".$bulan."' and year(periode)='".$tahun."' order by presensi_id desc")->result_array();
		// Start date
		$date = $tahun."-".$bulan."-01";
		$endDate=strtotime($date) ;
		
		$dataTemp = array();
		$jamKerjaDefault = $this->model->GetJamKerjaDefault("")->first_row();
		while (strtotime($date) < strtotime("+1 month",$endDate)) {
                //echo "$date\n";
				if(empty($data_absensi)){
					$dataTemp[$date]['karyawan_id'] =$data_karyawan->nippos; 
					$dataTemp[$date]['nama_kar'] =$data_karyawan->nama_kar; 
					$dataTemp[$date]['tanggal'] =$date; 
					$dataTemp[$date]['jam_kerja_masuk'] = $jamKerjaDefault->jam_kerja_masuk; 
					$dataTemp[$date]['jam_masuk'] ="";
					$dataTemp[$date]['jam_kerja_keluar'] =$jamKerjaDefault->jam_kerja_keluar; 
					$dataTemp[$date]['jam_keluar'] = ""; 
					$hari = date('D', strtotime($date));
					
					if($hari=="Sun" ||$hari=="Sat" )
					{
						$dataTemp[$date]['warna'] = "gray";
					}
					else
					{
						$dataTemp[$date]['warna'] = "white";
					}
				}
				else{
					foreach($data_absensi as $row){
						if($date==$row['tanggal'])
						{
							$dataTemp[$date]['karyawan_id'] =$row['karyawan_id']; 
							$dataTemp[$date]['nama_kar'] =$row['nama_kar']; 
							$dataTemp[$date]['tanggal'] =$row['tanggal']; 
							$dataTemp[$date]['jam_kerja_masuk'] = date('H:i:s', strtotime($row['jam_kerja_masuk']));
							$dataTemp[$date]['jam_masuk'] =$row['jam_masuk']==null?"":date('H:i:s', strtotime($row['jam_masuk']));
							$dataTemp[$date]['jam_kerja_keluar'] =date('H:i:s', strtotime($row['jam_kerja_keluar']));
							$dataTemp[$date]['jam_keluar'] = $row['jam_keluar']==null ?"":date('H:i:s', strtotime($row['jam_keluar']));
							$hari = date('D', strtotime($date));

							if($hari=="Sun" || $hari=="Sat" )
							{
								$dataTemp[$date]['warna'] = "gray";
							}
							else
							{
								$dataTemp[$date]['warna'] = "white";
							}
							break;
						}
						else
						{
							$dataTemp[$date]['karyawan_id'] =$data_karyawan->nippos; 
							$dataTemp[$date]['nama_kar'] =$data_karyawan->nama_kar; 
							$dataTemp[$date]['tanggal'] =$date; 
							$dataTemp[$date]['jam_kerja_masuk'] = $jamKerjaDefault->jam_kerja_masuk; 
							$dataTemp[$date]['jam_masuk'] ="";
							$dataTemp[$date]['jam_kerja_keluar'] =$jamKerjaDefault->jam_kerja_keluar; 
							$dataTemp[$date]['jam_keluar'] = ""; 
							
							$hari = date('D', strtotime($date));
	
							if($hari=="Sun" || $hari=="Sat" )
							{
								$dataTemp[$date]['warna'] = "gray";
							}
							else
							{
								$dataTemp[$date]['warna'] = "white";
							}
						}
					}
				}
                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
		}
		
		$indMonthName =  $this->model->GetIndonesianDateNeame($bulan);
		$indMonthNameNow =  $this->model->GetIndonesianDateNeame(date('m'));
		//var_dump($dataTemp );die();
		$data = array(
			'data_absensi' => $dataTemp,
			'data_karyawan' => $data_karyawan,	
			'nama' => $this->session->userdata('nama'),	
			'id_kar' => $idKar,
			'bulan' => $bulan,	
			'tahun' => $tahun,	
			'indMonthName' => $indMonthName,	
			'indMonthNameNow' => $indMonthNameNow,			
		);
		
		$this->load->view('laporan/data_cetakpdf', $data);
	}	
	
	public function cetak_presensi_csv()
		{
		$idKar = $this->uri->segment(3);
		$bulan=$this->uri->segment(4);
		$tahun=$this->uri->segment(5);
		
		$data_karyawan = $this->model->GetKaryawan("where id_kar=".$idKar."")->first_row();
		$data_absensi = $this->model->GetKaryawanJabAbs("where id_kar=".$idKar." and month(periode)='".$bulan."' and year(periode)='".$tahun."' order by presensi_id desc")->result_array();
		// Start date
		$date = $tahun."-".$bulan."-01";
		$endDate=strtotime($date) ;
		
		$dataTemp = array();
		$jamKerjaDefault = $this->model->GetJamKerjaDefault("")->first_row();
		while (strtotime($date) < strtotime("+1 month",$endDate)) {
                //echo "$date\n";
				if(empty($data_absensi)){
					$dataTemp[$date]['karyawan_id'] =$data_karyawan->nippos; 
					$dataTemp[$date]['nama_kar'] =$data_karyawan->nama_kar; 
					$dataTemp[$date]['tanggal'] =$date; 
					$dataTemp[$date]['jam_kerja_masuk'] = $jamKerjaDefault->jam_kerja_masuk; 
					$dataTemp[$date]['jam_masuk'] ="";
					$dataTemp[$date]['jam_kerja_keluar'] =$jamKerjaDefault->jam_kerja_keluar; 
					$dataTemp[$date]['jam_keluar'] = ""; 
					$hari = date('D', strtotime($date));
					
					if($hari=="Sun" ||$hari=="Sat" )
					{
						$dataTemp[$date]['warna'] = "gray";
					}
					else
					{
						$dataTemp[$date]['warna'] = "white";
					}
				}
				else{
					foreach($data_absensi as $row){
						if($date==$row['tanggal'])
						{
							$dataTemp[$date]['karyawan_id'] =$row['karyawan_id']; 
							$dataTemp[$date]['nama_kar'] =$row['nama_kar']; 
							$dataTemp[$date]['tanggal'] =$row['tanggal']; 
							$dataTemp[$date]['jam_kerja_masuk'] = date('H:i:s', strtotime($row['jam_kerja_masuk']));
							$dataTemp[$date]['jam_masuk'] =$row['jam_masuk']==null?"":date('H:i:s', strtotime($row['jam_masuk']));
							$dataTemp[$date]['jam_kerja_keluar'] =date('H:i:s', strtotime($row['jam_kerja_keluar']));
							$dataTemp[$date]['jam_keluar'] = $row['jam_keluar']==null ?"":date('H:i:s', strtotime($row['jam_keluar']));
							$hari = date('D', strtotime($date));

							if($hari=="Sun" || $hari=="Sat" )
							{
								$dataTemp[$date]['warna'] = "gray";
							}
							else
							{
								$dataTemp[$date]['warna'] = "white";
							}
							break;
						}
						else
						{
							$dataTemp[$date]['karyawan_id'] =$data_karyawan->nippos; 
							$dataTemp[$date]['nama_kar'] =$data_karyawan->nama_kar; 
							$dataTemp[$date]['tanggal'] =$date; 
							$dataTemp[$date]['jam_kerja_masuk'] = $jamKerjaDefault->jam_kerja_masuk; 
							$dataTemp[$date]['jam_masuk'] ="";
							$dataTemp[$date]['jam_kerja_keluar'] =$jamKerjaDefault->jam_kerja_keluar; 
							$dataTemp[$date]['jam_keluar'] = ""; 
							
							$hari = date('D', strtotime($date));
	
							if($hari=="Sun" || $hari=="Sat" )
							{
								$dataTemp[$date]['warna'] = "gray";
							}
							else
							{
								$dataTemp[$date]['warna'] = "white";
							}
						}
					}
				}
                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
		}
		
		$nameClear = preg_replace('/^([^,]*).*$/', '$1', $data_karyawan->nama_kar);
		$indMonthName =  $this->model->GetIndonesianDateNeame($bulan);
		
		$fileName= "PresensiTLH_".$nameClear."_".$indMonthName."".$tahun.".xls";
		//$fileName= "PresensiTLH.xls";
		
		//var_dump($fileName );die();
		$data = array(
			'data_absensi' => $dataTemp,
			'data_karyawan' => $data_karyawan,	
			'nama' => $this->session->userdata('nama'),	
			'id_kar' => $idKar,
			'bulan' => $bulan,	
			'tahun' => $tahun,	
			'indMonthName' => $indMonthName,
			'fileName' =>$fileName	,		
		);
		
		$this->load->view('laporan/data_cetakcsv', $data);
	}	
	
	public function cetak_sertifikat_pdf()
	{
		$idTest = $this->uri->segment(3);
		$dataSertifikat = $this->model->GetDataTest(" and a.id_test=$idTest")->result_array();
		// $noSertifikatD=$this->model->GetSertifiatInc();
	// //var_dump($noSertifikatD);die();
	
		// $noSertifikat =SERTIFIKAT_NO;
		// if($noSertifikatD->no_sertifikat !=  null)
		// {
			// $noSertifikat = $noSertifikatD->no_sertifikat;
			// $noSertifikat = $noSertifikat+1;
		// }

		$data = array(
			'data_sertifikat' => $dataSertifikat,
			'idTest' => $idTest
			//'no_sertifikat' => $noSertifikat
		);
		
		//var_dump($data);die();
		$this->load->view('laporan/data_cetaksertifikatpdf', $data);
	}
	
	public function cetak_test_csv()
		{
		$tipeTes = $this->input->post("tipeTes");
		$tanggalTesEx1 = $this->input->post("tanggalTesEx1");
		$tanggalTesEx2 = $this->input->post("tanggalTesEx2");
		$waktuTes = $this->input->post("waktuTes");
		$keterangan = $this->input->post("keterangan");
		
		$where ="";
		$count=0;
		
		if($tipeTes !="")
		{
			$where .=" and type_test = '$tipeTes'";
			$count++;
		}		
		
		if($tanggalTesEx1 !="")
		{
			if($count > 0  && $waktuTes == ""){
			
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tanggal_test) = '$tanggalTesEx1'";
				}
			}
			else if($count == 0  && $waktuTes == "")
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tanggal_test) = '$tanggalTesEx1'";
				}
			}
			
			elseif($count > 0  && $waktuTes != ""){

				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' and Time(tanggal_test) = '$waktuTes' ";
				}
				else
				{
					$tanggalTestAll = $tanggalTes." ".$waktuTes;
					$where .=" and tanggal_test = '$tanggalTestAll'";
				}
			}
			else if($count == 0  && $waktuTes != "")
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' and Time(tanggal_test) = '$waktuTes' ";
				}
				else
				{
					$tanggalTestAll = $tanggalTes." ".$waktuTes;
					$where .=" and tanggal_test = '$tanggalTestAll'";
				
				}
			}
		}
		
		if($keterangan != "")
		{
			if($count > 0)
			{
				$where .= "and keterangan = $keterangan";
			}
			else
			{
				$where .= "and keterangan = $keterangan";
			}
		}
		
		$fileName= "Data_Test_EprT".date("Y-m-d h:i:sa");
		//var_dump($where);die();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_test' => $this->model->GetDataTest($where)->result_array(),
			'fileName' =>$fileName	,
		);

		$this->load->view('laporan/data_test_cetakcsv', $data);
	}	
	
	public function cetak_sertifikat_ecct_pdf()
	{
		$idTest = $this->uri->segment(3);
		$dataSertifikat = $this->model->GetDataTestEcct(" and a.id_test_ecct=$idTest")->result_array();

		$data = array(
			'data_sertifikat' => $dataSertifikat,
            'idTest' =>$idTest
		);
		
		//var_dump($data);die();
		$this->load->view('laporan/data_cetaksertifikatecctpdf', $data);
	}
	
	public function cetak_test_ecct_csv()
		{
		$tipeTes = $this->input->post("tipeTes");
		$tanggalTesEx1 = $this->input->post("tanggalTesEx1");
		$tanggalTesEx2 = $this->input->post("tanggalTesEx2");
		$waktuTes = $this->input->post("waktuTes");
		$keterangan = $this->input->post("keterangan");
		
		$where ="";
		$count=0;
		
		if($tipeTes !="")
		{
			$where .=" and type_test = '$tipeTes'";
			$count++;
		}		
		
		if($tanggalTesEx1 !="")
		{
			if($count > 0  && $waktuTes == ""){
			
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tanggal_test) = '$tanggalTesEx1'";
				}
			}
			else if($count == 0  && $waktuTes == "")
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' ";
				}
				else
				{
					$where .=" and Date(tanggal_test) = '$tanggalTesEx1'";
				}
			}
			
			elseif($count > 0  && $waktuTes != ""){

				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' and Time(tanggal_test) = '$waktuTes' ";
				}
				else
				{
					$tanggalTestAll = $tanggalTes." ".$waktuTes;
					$where .=" and tanggal_test = '$tanggalTestAll'";
				}
			}
			else if($count == 0  && $waktuTes != "")
			{
				if($tanggalTesEx2 !="")
				{
					$where .=" and Date(tanggal_test) between '$tanggalTesEx1' and '$tanggalTesEx2' and Time(tanggal_test) = '$waktuTes' ";
				}
				else
				{
					$tanggalTestAll = $tanggalTes." ".$waktuTes;
					$where .=" and tanggal_test = '$tanggalTestAll'";
				
				}
			}
		}
		
		if($keterangan != "")
		{
			if($count > 0)
			{
				$where .= "and keterangan = $keterangan";
			}
			else
			{
				$where .= "and keterangan = $keterangan";
			}
		}
		$fileName= "Data_Test_Ecct".date("Y-m-d h:i:sa");
		//var_dump($where);die();
		$data = array(
			'nama' => $this->session->userdata('nama'),	
			'data_test' => $this->model->GetDataTestEcct($where)->result_array(),
			'fileName' =>$fileName,
			
		);

		$this->load->view('laporan/data_test_ecct_cetakcsv', $data);
	}

	//	Added by Agiel's Althur
    public function cek_qr($data=''){
	    $data = array('nama' => $this->session->userdata('nama'));
	    if (isset($_POST['serial']))
	    {
	        $serial = base64_decode($this->input->post('serial'));
	        $this->db->select('type_test, id_test');
	        $this->db->where('no_sertifikat',$serial);
	        $this->db->order_by('last_modified','DESC');
	        $q = $this->db->get('tb_cetak_sertifikat');

	        if ($q->num_rows()>=1)
	        {
	            $row = $q->row();
	            if ($row->type_test =='EPrT'){
	                $this->db->select('tanggal_test, nim_nik, fakultas, prodi, nama_lengkap, type_test, total');
	                $this->db->where('id_test',$row->id_test);
	                $q2 = $this->db->get('tb_test');
	                if ($q2->num_rows()==1)
	                {
                        $row2 = $q2->row();
                        $data['serial'] = $serial;
                        $data['tanggal_test']=$row2->tanggal_test;
                        $data['nim_nik']=$row2->nim_nik;
                        $data['fakultas']=$row2->fakultas;
                        $data['prodi']=$row2->prodi;
                        $data['nama_lengkap']=$row2->nama_lengkap;
                        $data['type_test']=$row2->type_test;
                        $data['total']=$row2->total;
                    }
                }
	            elseif ($row->type_test =='ECCT'){
                    $this->db->select('tanggal_test, nim_nik, fakultas, prodi, nama_lengkap, type_test, average');
                    $this->db->where('id_test',$row->id_test);
                    $q2 = $this->db->get('tb_test_ecct');
                    if ($q2->num_rows()==1)
                    {
                        $row2 = $q2->row();
                        $data['serial'] = $serial;
                        $data['tanggal_test']=$row2->tanggal_test;
                        $data['nim_nik']=$row2->nim_nik;
                        $data['fakultas']=$row2->fakultas;
                        $data['prodi']=$row2->prodi;
                        $data['nama_lengkap']=$row2->nama_lengkap;
                        $data['type_test']=$row2->type_test;
                        $data['total']=$row2->average;
                    }
                }
            }
        }
	    $this->load->view('laporan/cek_qr',$data);

    }
}
?>