<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('inc/head'); ?>

  </head>
  <body class="skin-blue">
  <!-- wrapper di bawah footer -->
    <div class="wrapper">
      
      <?php $this->load->view('inc/head2'); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php $this->load->view('inc/sidebar'); ?>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <b>DASHBOARD</b>
          </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            
            <div class="col-lg-12 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>LANGUAGE CENTER</h3>
                  <p><b><h3>TELKOM UNIVERSITY</h3></b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-users"></i>
                </div>
               <!-- <a href="#" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a> -->
              </div>
            </div><!-- ./col -->

          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
              <!-- Chat box -->
              <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-comments-o"></i>
                  <!-- <h3 class="box-title">Info LAC</h3><br> -->
                </div>
                <div class="box-body chat" id="chat-box">
                  <!-- chat item -->
                  <div class="item">
                    <!-- <img src="<?php echo base_url(); ?>assets/dist/img/user4-128x128.jpg" alt="user image" class="online"/> -->
                    <p class="message">
                      <a href="#" class="name">
                        <!-- <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small> -->
                        ADMIN
                      </a>
                      Welcome to LaC Insite 
                    </p>
                  </div>
				  <center><h1>OUR SERVICES</h1></center>
				<center> <p> <video width="600px" height="350px" controls>
						<source src="assets/upload/lac_profile.mp4" type="video/mp4">
					</video></p></center>
				  <center><p><img width=300 height=360 src='assets/upload/eprt.png'/>
				  <img width=300 height=360 src='assets/upload/harga_test.png'/>
				  </p></center>
				  <center><img width=300 height=360 src='assets/upload/harga_kursus.png'/>
				  <img width=300 height=360 src='assets/upload/harga_kursus_2.png'/>
				  <img width=300 height=360 src='assets/upload/harga_kursus_3.png'/>
				  <img width=300 height=360 src='assets/upload/harga_kursus_4.png'/>
				  <img width=300 height=360 src='assets/upload/harga_kursus_5.png'/>
				  <img width=300 height=360 src='assets/upload/harga_kursus_6.png'/></center>
					
				  <!-- /.item -->
                  <!-- chat item -->
                 <!--  <div class="item">
                    <img src="<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg" alt="user image" class="offline"/>
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                        Alexander Pierce
                      </a>
                      I would like to meet you to discuss the latest news about
                      the arrival of the new theme. They say it is going to be one the
                      best themes on the market
                    </p>
                  </div><!-- /.item -->
                  <!-- chat item -->
                 <!-- <div class="item">
                    <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" alt="user image" class="offline"/>
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                        Susan Doe
                      </a>
                      I would like to meet you to discuss the latest news about
                      the arrival of the new theme. They say it is going to be one the
                      best themes on the market
                    </p>
                  </div><!-- /.item -->
                </div><!-- /.chat -->
              </div><!-- /.box (chat box) -->
            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable">

            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <!-- <b>Version</b> 2.0 -->
        </div> 
        <strong>Copyright &copy; 2015 <a href="#"></a></strong>
      </footer>
    </div><!-- ./wrapper -->
    <?php $this->load->view('inc/footer', TRUE); ?>
  </body>
</html>